<?php

header('Content-Type: application/json');

error_reporting(E_ERROR | E_PARSE);
ini_set("display_errors", 1);
ini_set('session.cookie_lifetime',84600);
ini_set('session.gc_maxlifetime',84600);

session_start();

require 'app/init.php';
