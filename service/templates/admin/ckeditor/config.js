/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

var config;

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.allowedContent = true;	
	config.extraPlugins = 'youtube';
	
	config.filebrowserBrowseUrl = '/service/templates/admin/ckfinder/ckfinder.html';
	config.filebrowserImageBrowseUrl = '/service/templates/admin/ckfinder/ckfinder.html?type=Images';
	config.filebrowserFlashBrowseUrl = '/service/templates/admin/ckfinder/ckfinder.html?type=Flash';
	config.filebrowserUploadUrl = '/service/templates/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
	config.filebrowserImageUploadUrl = '/service/templates/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
	config.filebrowserFlashUploadUrl = '/service/templates/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';	
};
