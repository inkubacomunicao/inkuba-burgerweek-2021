<?php

namespace app\model;

use lib\Db\Crud as Crud;

class Paginas Extends Crud
{
    /**
     * @var string $table Nome da tabela
     */
    protected $table = 'tbl_paginas';
    
    /**
     * @var string $pk Chave primária da tabela
     */
    protected $pk    = 'PaginasID';
	
    /**
     * @var string $status Status do registro na tabela (0 = excluido)
     */	
	protected $status = 'pag_status';
	
}
