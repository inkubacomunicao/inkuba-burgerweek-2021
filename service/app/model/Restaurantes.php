<?php

namespace app\model;

use lib\Db\Crud as Crud;

class Restaurantes Extends Crud
{
    /**
     * @var string $table Nome da tabela
     */
    protected $table = 'tbl_restaurantes';

    /**
     * @var string $pk Chave primária da tabela
     */
    protected $pk    = 'RestauranteID';

    /**
     * @var string $status Status do registro na tabela (0 = excluido)
     */
	protected $status = 'res_status';

}
