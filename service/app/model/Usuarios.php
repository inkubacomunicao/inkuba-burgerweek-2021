<?php

namespace app\model;

use lib\Db\Crud as Crud;

class Usuarios Extends Crud
{
    /**
     * @var string $table Nome da tabela
     */
    protected $table = 'tbl_usuarios';
    
    /**
     * @var string $pk Chave primária da tabela
     */
    protected $pk    = 'UsuarioID';
	
	protected $status = 'usu_status';
	
	protected $email = 'usu_email';
	
	
}
