<?php

namespace app\model;

use lib\Db\Crud as Crud;

class Menus Extends Crud
{
    /**
     * @var string $table Nome da tabela
     */
    protected $table = 'tbl_menus';
    
    /**
     * @var string $pk Chave primária da tabela
     */
    protected $pk     = 'MenusID';
	protected $status = 'men_status';
}
