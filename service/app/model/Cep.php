<?php

namespace app\model;

use lib\Db\Crud as Crud;

class Cep Extends Crud
{
    /**
     * @var string $table Nome da tabela
     */
    protected $table = 'tbl_cep';
    
    /**
     * @var string $pk Chave primária da tabela
     */
    protected $pk    = 'EnderecoID';
}
