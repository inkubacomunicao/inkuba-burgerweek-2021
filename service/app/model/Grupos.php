<?php

namespace app\model;

use lib\Db\Crud as Crud;

class Grupos Extends Crud
{
    /**
     * @var string $table Nome da tabela
     */
    protected $table = 'tbl_usuarios_grupo';
    
    /**
     * @var string $pk Chave primária da tabela
     */
    protected $pk     = 'UsuarioGrupoID';
	protected $status = 'gru_status';
}
