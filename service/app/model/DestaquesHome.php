<?php

namespace app\model;

use lib\Db\Crud as Crud;

class DestaquesHome Extends Crud
{
    /**
     * @var string $table Nome da tabela
     */
    protected $table = 'tbl_destaques_home';
    
    /**
     * @var string $pk Chave primária da tabela
     */
    protected $pk    = 'DestaquesHomeID';
	
    /**
     * @var string $status Status do registro na tabela (0 = excluido)
     */	
	protected $status = 'deh_status';
	
}
