<?php
/**
 * @package
 * @category
 * @subpackage
 *
 * @SWG\Resource(
 *   apiVersion="1.0.0",
 *   swaggerVersion="1.2",
 *   basePath="https://localhost/service",
 *   resourcePath="/admin",
 *   description="Operações Admin. Usuário",
 *   produces="['application/json']"
 * )
 */

use app\model\Usuarios;
use app\model\Grupos;

$usuarios  = new Usuarios(array(), $app->db);
$usuariosGrupo  = new Grupos(array(), $app->db);

/**
 *
 * @SWG\Api(
 *   path="/admin/usuarios",
 *   description="Listagem de usuários",
 *   @SWG\Operation(method="GET", summary="Listagem de usuários", type="void", nickname="listagemUsuarios",
 *      @SWG\ResponseMessage(code=500, message="Problema ao acessar destaques") 
 *   )
 * )
 */
$app->map('/admin/usuarios', function () use ($app, $usuarios, $usuariosGrupo){

    $params = $app->request;

    if ($params->isPost()) {
	
		// Usuario ja existe
		$R = $usuarios->findByEmail($params->post('usu_email'));
		if($R->cod == 200 && $R->qtd > 0) {
			$R->cod = 500;
			$R->res = 'Email já cadastrado!';
			
			echo json_encode($R, JSON_NUMERIC_CHECK);
			exit;
		}
	
		// Senhas digitadas nao conferem
		if($params->post('usu_senha') != $params->post('usu_repita_senha')) {
			$R->cod = 404;
			$R->res = 'Senhas não conferem!';
			
			echo json_encode($R, JSON_NUMERIC_CHECK);
			exit;
		}
		
		$campos				  = $params->post();
		$campos['usu_senha']  = $app->auth->hashPassword($campos['usu_senha']);

		unset($campos['usu_repita_senha']);
		
        $res = $usuarios->create($campos);

		echo json_encode($res, JSON_NUMERIC_CHECK);
	
	// Listagem de usuarios
    } else {
		$res = $usuarios->Query("SELECT UsuarioID AS 'ID', usu_nome AS 'Nome', usu_email AS 'Email'
										FROM tbl_usuarios 
										WHERE 
											usu_status = 1 AND UsuarioID <> 1
										ORDER BY 2");
		$colunas = array_keys($res->res[0]);
		
		$grp = $usuariosGrupo->findAll();
	
		$app->render('admin/usuarios.html.twig', array('linhas'=>$res->res, 'colunas'=>$colunas, 'grupos'=>$grp->res, 'pagina'=>'Usuários', 'rota'=>'usuarios'));

	}	
	
})->via('GET', 'POST')->name('listagem_usuarios');

/**
 *
 * @SWG\Api(
 *   path="/admin/usuarios/{id}",
 *   description="Dados do usuário",
 *   @SWG\Operation(method="GET", summary="Dados do usuário", type="array", nickname="dadosUsuario",
 *          @SWG\Parameter(
 *              name="id",
 *              description="id do usuário",
 *              required=true,
 *              type="string",
 *              paramType="path"
 *          )
 *       ),
 *      @SWG\ResponseMessage(code=500, message="Problema ao pegar os dados do usuário")
 *   )
 * )
 */
$app->get('/admin/usuarios/:id', function ($id) use ($app, $usuarios) {
    $res = $usuarios->findById($id, array('UsuarioID','usu_nome','usu_email','usu_grupos','usu_ativo'));

    echo json_encode($res, JSON_NUMERIC_CHECK);
})->name('busca_usuario');


/**
 *
 * @SWG\Api(
 *   path="/admin/usuarios/{id}",
 *   description="Remover usuário",
 *   @SWG\Operation(method="DELETE", summary="Remover usuário", type="void", nickname="removerUsuario",
 *      @SWG\ResponseMessage(code=500, message="Problema ao remover usuário")
 *   )
 * )
 */
$app->delete('/admin/usuarios/:id', function ($id) use ($app, $usuarios, $projetos) {
    $res = $usuarios->delete('UsuarioID = ' . $id);

	echo json_encode($res, JSON_NUMERIC_CHECK);
	
});

/**
 *
 * @SWG\Api(
 *   path="/admin/usuarios/{id}",
 *   description="Editar dados do usuário",
 *   @SWG\Operation(method="POST", summary="Editar usuário", type="void", nickname="editaUsuario",
 *      @SWG\Parameters(
 *          @SWG\Parameter(
 *              name="id",
 *              description="id do usuário",
 *              paramType="path",
 *              required=true,
 *              type="string"
 *          ),
 *          @SWG\Parameter(
 *              name="nome",
 *              description="Nome",
 *              required=false,
 *              type="string",
 *              paramType="form"
 *          ),
 *          @SWG\Parameter(
 *              name="email",
 *              description="E-mail",
 *              required=false,
 *              type="string",
 *              paramType="form"
 *          ),
 *          @SWG\Parameter(
 *              name="senha",
 *              description="Senha",
 *              required=false,
 *              type="string",
 *              paramType="form"
 *          ),
  *          @SWG\Parameter(
 *              name="confirma_senha",
 *              description="Confirmação Senha",
 *              required=false,
 *              type="string",
 *              paramType="form"
 *          )
 *      ),
 *      @SWG\ResponseMessage(code=500, message="Problema ao editar usuário")
 *   )
 * )
 */
$app->post('/admin/usuarios/:id', function ($id) use ($app, $usuarios) {
    $params = $app->request;

	$campos = $app->request->post();
	
    $res = $usuarios->findById($id);
    if ($res->cod == 404) {
        $app->notFound();
        exit;
    }

    $senha  = trim($params->post('usu_senha'));
    $confirmacaoSenha = trim($params->post('usu_repita_senha'));

    if (!empty($senha) && $senha !== 'undefined') {
        if ($senha !== $confirmacaoSenha ) {
            $R->cod = 500;
			$R->res = "Senhas não conferem.";

            echo json_encode($R, JSON_NUMERIC_CHECK);
			exit;
        } 

        $campos['usu_senha'] = $app->auth->hashPassword($senha);
    } else {
		unset($campos['usu_senha']);
	}
	
	unset($campos['usu_repita_senha']);
	unset($campos['UsuarioID']);
	
    $R = $usuarios->save($campos, 'UsuarioID = ' . $id);
	
	if($R->cod == 200) {
		$R->res = "Dados salvos com sucesso.";
	} else {
		$R->res = "Houve um erro. Tente novamente.";
	}
	
	
	echo json_encode($R, JSON_NUMERIC_CHECK);
	
});

/**
 *
 * @SWG\Api(
 *   path="/admin/esqueci-senha",
 *   description="Enviar email com link para trocar senha",
 *   @SWG\Operation(method="POST", summary="Enviar e-mail para trocar senha", type="void", nickname="esqueciSenha",
 *      @SWG\Parameters(
 *          @SWG\Parameter(
 *              name="email",
 *              description="Email para recuperar a senha",
 *              required=true,
 *              type="string",
 *              paramType="form"
 *          )
 *       ),
 *      @SWG\ResponseMessage(code=404, message="O e-mail informado não consta no sistema."),
 *      @SWG\ResponseMessage(code=400, message="Não foi possível salvar o token do usuário.")
 *   )
 * )
 */
$app->post('/admin/esqueci-senha', function () use ($app, $usuarios, $projetos) {
    $params = $app->request;
    $view   = $app->view();
    $email  = $app->request->post('email');

    // verifica se e-mail está cadastrado
    $usuario = $usuarios->findOne(
        array('email' => $email),
        'email = :email'
    );

    $projeto = $projetos->findOne(
         array('projeto' => 'Gael'),
        'projeto = :projeto'
    );

    if ($usuario->cod == 200) {
        $tokenConfirmacao = $app->auth->geraTokenConfirmacao();

        $usuarios->tokenConfirmacao = $tokenConfirmacao;

        $res = $usuarios->save(array('id' => $usuario->res['id']), 'id = :id');

        if ($res->cod == 200) {
            $urlTrocaSenha = $app->config('dominio.frontend') . '/admin/troca-senha/' . $tokenConfirmacao;

            $view->setData('link', $urlTrocaSenha);

            $templateEmail = $view->render('esqueci-senha.html.twig');

            $app->mailer->send(\Swift_Message::newInstance()
                                ->setSubject('[Gael] Esqueci a minha senha')
                                ->setFrom(array($projeto->res['contato_email']))
                                ->setTo(array($params->post('email')))
                                ->setBody($templateEmail,'text/html'));

            $res = array('res' => 'Favor verificar o seu e-mail.');
            echo json_encode($res);

            return;
        }

        $app->response->setStatus(400);

        echo json_encode(array(
            'res' => 'Não foi possível salvar o token do usuário.',
            'cod' => 400,
        ));

        return;
    }

    $app->response->setStatus(404);

    return json_encode(array(
        'res' => 'O e-mail informado não consta no sistema.',
        'cod' => 404,
    ));
});

/**
 *
 * @SWG\Api(
 *   path="/admin/troca-senha",
 *   description="Troca senha do usuário",
 *   @SWG\Operation(method="POST", summary="Trocar senha usuário", type="void", nickname="trocaSenhaUsuario",
 *      @SWG\Parameters(
 *          @SWG\Parameter(
 *              name="token",
 *              description="token de confirmação",
 *              paramType="form",
 *              required=true,
 *              type="string"
 *          ),
 *          @SWG\Parameter(
 *              name="senha",
 *              description="Senha",
 *              required=true,
 *              type="string",
 *              paramType="form"
 *          ),
 *          @SWG\Parameter(
 *              name="confirma-senha",
 *              description="Confirmação senha",
 *              required=true,
 *              type="string",
 *              paramType="form"
 *          )
 *       ),
 *      @SWG\ResponseMessage(code=500, message="Problema ao trocar senha do usuário"),
 *      @SWG\ResponseMessage(code=404, message="O usuário com token nnnn não foi encontrado."),
 *      @SWG\ResponseMessage(code=400, message="As senhas não conferem.")
 *   )
 * )
 */
 
$app->post('/admin/usuarios-alterar-senha', function () use ($app, $usuarios) {

    $params = $app->request;

    $senha            = trim($params->post('usu_senha_nova'));
    $confirmacaoSenha = trim($params->post('usu_senha_repita_nova'));

    if ($senha !== $confirmacaoSenha ) {
        //$app->response->setStatus(400);
		$R->cod = 500;
		$R->res = 'As senhas não conferem.';

		echo json_encode($R, JSON_NUMERIC_CHECK);
		exit;

    }

	if(trim($params->post('UsuarioID')) !== $_SESSION['auth_user']['UsuarioID']) {
		$R->cod = 500;
		$R->res = 'Você não parece ser a mesma pessoa.';

		echo json_encode($R, JSON_NUMERIC_CHECK);
		exit;

	}
	
    $campos['usu_senha'] = $app->auth->hashPassword($senha);

    $res = $usuarios->save($campos, 'UsuarioID = ' . $_SESSION['auth_user']['UsuarioID']);
	
	if($res->cod == 200) {
		$R->res = 'Senha alterada com sucesso';
	}
	
    echo json_encode($res, JSON_NUMERIC_CHECK);
	
});
