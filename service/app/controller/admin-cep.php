<?php
/**
 * @package
 * @since  Tue, 29 Apr 14 16:45:02 -0300
 * @category
 * @subpackage
 *
 * @SWG\Resource(
 *   apiVersion="1.0.0",
 *   swaggerVersion="1.2",
 *   basePath="https://localhost/service",
 *   resourcePath="/admin",
 *   description="Operações Admin. CEP",
 *   produces="['application/json']"
 * )
 */

use app\model\Cep;

$cep = new Cep(array(), $app->db);

/**
 *
 * @SWG\Api(
 *   path="/admin/cep/{uf}",
 *   description="Cidades da UF informada",
 *   @SWG\Operation(method="GET", summary="Listagem de cidades", type="void", nickname="cidades",
 *      @SWG\ResponseMessage(code=500, message="Problemas")
 *   )
 * )
 */
$app->get('/admin/cep/:uf', function ($uf) use ($app, $cep) {
    $R = $cep->Query("SELECT DISTINCT(end_cidade) AS cidade
						FROM tbl_cep
						WHERE end_uf = '" . $uf . "'
						ORDER BY cidade");
						
	for($i=0; $i < sizeof($R->res); $i++) {
		$Res .= "<option value='" . $R->res[$i]['cidade'] . "'>" . $R->res[$i]['cidade'] . "</option>";
	}
	
	echo $Res;
});