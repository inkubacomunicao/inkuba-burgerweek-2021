<?php
/**
 * @package
 * @since  Tue, 29 Apr 14 16:45:02 -0300
 * @category
 * @subpackage
 *
 * @SWG\Resource(
 *   apiVersion="1.0.0",
 *   swaggerVersion="1.2",
 *   basePath="https://localhost/admin",
 *   resourcePath="/destaques/home",
 *   description="Operações Admin. Destaques",
 *   produces="['application/json']"
 * )
 */

use app\model\DestaquesHome;

$destaquesHome = new DestaquesHome(array(), $app->db);

/**
 *
 * @SWG\Api(
 *   path="/admin/destaques/home",
 *   description="Cadastrar Destaques",
 *   @SWG\Operation(method="POST", summary="Cadastrar Destaques", notes="", type="void", nickname="cadastradestaques",
 *      @SWG\Parameters(
 *    @SWG\Parameter(name="des_nome", description="", paramType="form", required=true, type="string"), *    @SWG\Parameter(name="des_descricao", description="", paramType="form", required=false, type="string"), *    @SWG\Parameter(name="des_link", description="", paramType="form", required=false, type="string"), *    @SWG\Parameter(name="des_IdiomaID", description="", paramType="form", required=true, type="string"), *    @SWG\Parameter(name="des_imagem", description="", paramType="form", required=true, type="string"),
 *       ),
 *      @SWG\ResponseMessage(code=200, message="Sucesso"),
 *      @SWG\ResponseMessage(code=404, message="Não encontrado"),
 *      @SWG\ResponseMessage(code=500, message="Erro interno no servidor")
 *   )
 * )
 */
 // NOVO
$app->post('/admin/destaques/home', function () use ($app, $destaquesHome, $idiomas) {
    $campos = $app->request->post();

	// slug do nome
	$campos['deh_slug'] = $app->slug->slugify($campos['deh_nome']);

    $R = $destaquesHome->create($campos);

	if($R->cod == 200) {
		$R->res = 'Dados salvos com sucesso. Redirecionando...';
	} else {
		$R->res = 'Falha ao salvar dados';
	}

	echo json_encode($R, JSON_NUMERIC_CHECK);

});


/**
 *
 * @SWG\Api(
 *   path="/admin/destaques/home/{DestaquesID}",
 *   description="Editar Destaques",
 *   @SWG\Operation(method="POST", summary="Editar Destaques", notes="", type="void", nickname="editadestaques",
 *      @SWG\Parameters(
 *    @SWG\Parameter(name="DestaquesID", description="", paramType="form", required="true", type="string"), *    @SWG\Parameter(name="des_nome", description="", paramType="form", required=true, type="string"), *    @SWG\Parameter(name="des_descricao", description="", paramType="form", required=false, type="string"), *    @SWG\Parameter(name="des_link", description="", paramType="form", required=false, type="string"), *    @SWG\Parameter(name="des_IdiomaID", description="", paramType="form", required=true, type="string"), *    @SWG\Parameter(name="des_imagem", description="", paramType="form", required=true, type="string"),
 *       ),
 *      @SWG\ResponseMessage(code=200, message="Sucesso"),
 *      @SWG\ResponseMessage(code=404, message="Não encontrado"),
 *      @SWG\ResponseMessage(code=500, message="Erro interno no servidor")
 *   )
 * )
 */
 // EDITAR
$app->post('/admin/destaques/home/:id', function ($id) use ($app, $destaquesHome, $idiomas) {
	$campos  = $app->request->post();

	// slug do nome
	$campos['deh_slug'] = $app->slug->slugify($campos['deh_nome']);

    $res = $destaquesHome->findById($id);

    if ($res->cod == 404) {
        $app->notFound();
        exit;
    }

	unset($campos['DestaquesID']);

	//$R = $destaquesHome->save($campos, 'DestaquesHomeID = ' . $id);
	$R = $destaquesHome->Query("UPDATE tbl_destaques_home SET
									deh_nome = '" . $campos['deh_nome'] . "',
									deh_slug = '" . $campos['deh_slug'] . "',
									deh_descricao = '" . $campos['deh_descricao'] . "',
									deh_link = '" . $campos['deh_link'] . "',
									deh_imagem = '" . $campos['deh_imagem'] . "',
                                    deh_imagem_mobile = '" . $campos['deh_imagem_mobile'] . "',
									deh_ativo = '" . $campos['deh_ativo'] . "',
                                    deh_ordem = '" . $campos['deh_ordem'] . "'
									WHERE DestaquesHomeID = " . $id);

	if($R->cod == 200) {
		$R->res = "Dados salvos com sucesso. Redirecionando...";
	} else {
		$R->res = "Houve um erro. Tente novamente.";
	}

	echo json_encode($R, JSON_NUMERIC_CHECK);

});

/**
 *
 * @SWG\Api(
 *   path="/admin/destaques/home",
 *   description="Listar Destaques",
 *   @SWG\Operation(method="POST", summary="Listar Destaques", notes="", type="void", nickname="listadestaques",
 *      @SWG\ResponseMessage(code=200, message="Sucesso"),
 *      @SWG\ResponseMessage(code=404, message="Não encontrado"),
 *      @SWG\ResponseMessage(code=500, message="Erro interno no servidor")
 *   )
 * )
 */
 // Listar
$app->get('/admin/destaques/home', function () use ($app, $destaquesHome, $idiomas, $equipes) {
    $res = $destaquesHome->Query("SELECT DestaquesHomeID AS 'ID', deh_nome AS 'Nome', deh_ativo AS 'Ativo', deh_ordem AS 'Ordem'
									FROM tbl_destaques_home
									WHERE deh_status = 1
                                    ORDER BY deh_ordem DESC");

    $colunas = array_keys($res->res[0]);

    $app->render('admin/destaques-home.html.twig', array('linhas'=>$res->res, 'colunas'=>$colunas, 'pagina'=>'Destaques da Home', 'rota'=>'destaques/home'));
});

/**
 *
 * @SWG\Api(
 *   path="/admin/destaques/home/{DestaquesID}",
 *   description="Listar Destaques",
 *   @SWG\Operation(method="POST", summary="Listar Destaques", notes="", type="void", nickname="listadestaques",
 *      @SWG\Parameters(
 *          @SWG\Parameter(
 *              name="DestaquesID",
 *              description="ID no banco de dados (DestaquesID)",
 *              paramType="path",
 *              required=true,
 *              type="string"
 *          ),
 *      ),
 *      @SWG\ResponseMessage(code=200, message="Sucesso"),
 *      @SWG\ResponseMessage(code=404, message="Não encontrado"),
 *      @SWG\ResponseMessage(code=500, message="Erro interno no servidor")
 *   )
 * )
 */
// Dados
$app->get('/admin/destaques/home/:id', function ($id) use ($app, $destaquesHome) {
    $R = $destaquesHome->Query("SELECT *
								FROM tbl_destaques_home
								WHERE deh_status = 1 AND DestaquesHomeID = " . $id, 'unica');

	echo json_encode($R, JSON_NUMERIC_CHECK);
});

/**
 *
 * @SWG\Api(
 *   path="/admin/destaques/home/{DestaquesID}",
 *   description="Deletar Destaques",
 *   @SWG\Operation(method="POST", summary="Deletar Destaques", notes="", type="void", nickname="deletedestaques",
 *      @SWG\Parameters(
 *          @SWG\Parameter(
 *              name="DestaquesID",
 *              description="ID no banco de dados (DestaquesID)",
 *              paramType="path",
 *              required=true,
 *              type="string"
 *          ),
 *      ),
 *      @SWG\ResponseMessage(code=200, message="Sucesso"),
 *      @SWG\ResponseMessage(code=404, message="Não encontrado"),
 *      @SWG\ResponseMessage(code=500, message="Erro interno no servidor")
 *   )
 * )
 */
// Delete
$app->delete('/admin/destaques/home/:id', function ($id) use ($app, $destaquesHome) {
    $R = $destaquesHome->delete('DestaquesHomeID = ' . $id);

	echo json_encode($R, JSON_NUMERIC_CHECK);
});

/**
 *
 * @SWG\Api(
 *   path="/admin/destaques/home/imagem/{ArquivoID}",
 *   description="Imagem do Destaques",
 *   @SWG\Operation(method="POST", summary="Imagem do Destaques", notes="", type="void", nickname="imagemdestaques",
 *      @SWG\Parameters(
 *          @SWG\Parameter(
 *              name="ArquivoID",
 *              description="ID no banco de dados (ArquivoID)",
 *              paramType="path",
 *              required=true,
 *              type="string"
 *          ),
 *      ),
 *      @SWG\ResponseMessage(code=200, message="Sucesso"),
 *      @SWG\ResponseMessage(code=404, message="Não encontrado"),
 *      @SWG\ResponseMessage(code=500, message="Erro interno no servidor")
 *   )
 * )
 */
// Imagem
$app->get('/admin/destaques/home/imagem/:id', function ($id) use ($app, $destaquesHome) {
    $R = $destaquesHome->Query("SELECT CONCAT('" . (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['SERVER_NAME'] . "/service/web/uploads/', arq_extensao, '/', arq_nome) AS arq_nome, arq_extensao
								FROM tbl_arquivos
								WHERE ArquivoID = " . $id, "unica");

	echo json_encode($R->res, JSON_NUMERIC_CHECK);
});
