<?php
/**
 * @package
 * @since  Mon, 28 Apr 14 18:51:46 -0300
 * @category
 * @subpackage
 *
 * @SWG\Resource(
 *   apiVersion="1.0.0",
 *   swaggerVersion="1.2",
 *   basePath="https://localhost/admin",
 *   resourcePath="/uploads",
 *   description="Operações de Upload",
 *   produces="['text/html']"
 * )
 */

use app\model\Arquivos;

$arquivos = new Arquivos(array(), $app->db);

/**
 *
 * @SWG\Api(
 *   path="/admin/uploads",
 *   description="Uploads",
 *   @SWG\Operation(method="POST", summary="Upload", type="void", nickname="UploadArquivos",
 *      @SWG\Parameters(
 *          @SWG\Parameter(
 *              name="arquivo",
 *              description="Arquivo para upload",
 *              paramType="form",
 *              required=true,
 *              type="string"
 *          )
 *      ),
 *      @SWG\ResponseMessage(code=500, message="Problema ao efetuar login")
 *   )
 * )
 */
$app->post('/admin/uploads', function () use ($app) {

    if ($app->request->isPost()) {

		if($_FILES['Filedata']['size'] > 0) {
			$uploader = $app->uploader;
			$uploader->setArquivo('Filedata');
			$uploader->setTipo($app->request->post('des_tipo'));
			$imagem = $uploader->salva();
			$destaques->des_imagem = $imagem->res['ArquivoID'];

			$R->cod = 200;
			$R->ArquivoID = $imagem->res['ArquivoID'];
			$R->arq_nome  = (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['SERVER_NAME'] . '/service/web/uploads/' . $imagem->res['arq_extensao'] . '/' . $imagem->res['arq_nome'];
			$R->arq_extensao  = $imagem->res['arq_extensao'];

		} else {
			$destaques->des_imagem = 0;

			$R->cod = 500;
			$R->ArquivoID = 0;

		}

		echo json_encode($R, JSON_NUMERIC_CHECK);

    }
});

/**
 *
 * @SWG\Api(
 *   path="/admin/uploads/delete",
 *   description="Uploads",
 *   @SWG\Operation(method="POST", summary="Upload", type="void", nickname="UploadArquivosDelete",
 *      @SWG\Parameters(
 *          @SWG\Parameter(
 *              name="arquivo",
 *              description="Arquivo para deletar",
 *              paramType="form",
 *              required=true,
 *              type="string"
 *          )
 *      ),
 *      @SWG\ResponseMessage(code=500, message="Problema ao efetuar login")
 *   )
 * )
 */
$app->post('/admin/uploads/delete', function () use ($app, $arquivos) {
    $R = $arquivos->delete('ArquivoID = ' . $app->request->post('ArquivoID'));

	echo json_encode($R, JSON_NUMERIC_CHECK);

});
