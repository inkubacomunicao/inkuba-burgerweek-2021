<?php
/**
 * @package
 * @since  Tue, 29 Apr 14 16:45:02 -0300
 * @category
 * @subpackage
 *
 * @SWG\Resource(
 *   apiVersion="1.0.0",
 *   swaggerVersion="1.2",
 *   basePath="https://localhost/admin",
 *   resourcePath="/restaurantes",
 *   description="Operações Admin. Restaurante",
 *   produces="['application/json']"
 * )
 */

use app\model\Restaurantes;

$restaurantes = new Restaurantes(array(), $app->db);

/**
 *
 * @SWG\Api(
 *   path="/admin/restaurantes",
 *   description="Cadastrar Restaurante",
 *   @SWG\Operation(method="POST", summary="Cadastrar Restaurante", notes="", type="void", nickname="cadastrarestaurantes",
 *      @SWG\Parameters(
 *    @SWG\Parameter(name="des_nome", description="", paramType="form", required=true, type="string"), *    @SWG\Parameter(name="des_descricao", description="", paramType="form", required=false, type="string"), *    @SWG\Parameter(name="des_link", description="", paramType="form", required=false, type="string"), *    @SWG\Parameter(name="des_IdiomaID", description="", paramType="form", required=true, type="string"), *    @SWG\Parameter(name="des_imagem", description="", paramType="form", required=true, type="string"),
 *       ),
 *      @SWG\ResponseMessage(code=200, message="Sucesso"),
 *      @SWG\ResponseMessage(code=404, message="Não encontrado"),
 *      @SWG\ResponseMessage(code=500, message="Erro interno no servidor")
 *   )
 * )
 */
 // NOVO
$app->post('/admin/restaurantes', function () use ($app, $restaurantes) {
    $campos = $app->request->post();

	// slug do nome
	$campos['res_slug'] = $app->slug->slugify($campos['res_nome']);

    $R = $restaurantes->create($campos);

	if($R->cod == 200) {
		$R->res = 'Dados salvos com sucesso. Redirecionando...';
	} else {
		$R->res = 'Falha ao salvar dados';
	}

	echo json_encode($R, JSON_NUMERIC_CHECK);

});


/**
 *
 * @SWG\Api(
 *   path="/admin/restaurantes/{RestauranteID}",
 *   description="Editar Restaurante",
 *   @SWG\Operation(method="POST", summary="Editar Restaurante", notes="", type="void", nickname="editarestaurantes",
 *      @SWG\Parameters(
 *    @SWG\Parameter(name="RestauranteID", description="", paramType="form", required="true", type="string"), *    @SWG\Parameter(name="des_nome", description="", paramType="form", required=true, type="string"), *    @SWG\Parameter(name="des_descricao", description="", paramType="form", required=false, type="string"), *    @SWG\Parameter(name="des_link", description="", paramType="form", required=false, type="string"), *    @SWG\Parameter(name="des_IdiomaID", description="", paramType="form", required=true, type="string"), *    @SWG\Parameter(name="des_imagem", description="", paramType="form", required=true, type="string"),
 *       ),
 *      @SWG\ResponseMessage(code=200, message="Sucesso"),
 *      @SWG\ResponseMessage(code=404, message="Não encontrado"),
 *      @SWG\ResponseMessage(code=500, message="Erro interno no servidor")
 *   )
 * )
 */
 // EDITAR
$app->post('/admin/restaurantes/:id', function ($id) use ($app, $restaurantes) {
	$campos  = $app->request->post();

	// slug do nome
	$campos['res_slug'] = $app->slug->slugify($campos['res_nome']);

    $res = $restaurantes->findById($id);

    if ($res->cod == 404) {
        $app->notFound();
        exit;
    }

	unset($campos['RestauranteID']);

	//$R = $restaurantes->save($campos, 'RestauranteID = ' . $id);
	$R = $restaurantes->Query("UPDATE tbl_restaurantes SET
									res_nome = '" . $campos['res_nome'] . "',
									res_slug = '" . $campos['res_slug'] . "',
                                    res_endereco = '" . $campos['res_endereco'] . "',
                                    res_latitude = '" . $campos['res_latitude'] . "',
                                    res_longitude = '" . $campos['res_longitude'] . "',
									res_funcionamento = '" . $campos['res_funcionamento'] . "',
									res_pedido1 = '" . $campos['res_pedido1'] . "',
                                    res_pedido2 = '" . $campos['res_pedido2'] . "',
                                    res_pedido3 = '" . $campos['res_pedido3'] . "',
                                    res_pedido4 = '" . $campos['res_pedido4'] . "',
                                    res_pedido5 = '" . $campos['res_pedido5'] . "',
									res_imagem = '" . $campos['res_imagem'] . "',
                                    res_menu = '" . $campos['res_menu'] . "',
									res_ativo = '" . $campos['res_ativo'] . "'
									WHERE RestauranteID = " . $id);

	if($R->cod == 200) {
		$R->res = "Dados salvos com sucesso. Redirecionando...";
	} else {
		$R->res = "Houve um erro. Tente novamente.";
	}

	echo json_encode($R, JSON_NUMERIC_CHECK);

});

/**
 *
 * @SWG\Api(
 *   path="/admin/restaurantes",
 *   description="Listar Restaurante",
 *   @SWG\Operation(method="POST", summary="Listar Restaurante", notes="", type="void", nickname="listarestaurantes",
 *      @SWG\ResponseMessage(code=200, message="Sucesso"),
 *      @SWG\ResponseMessage(code=404, message="Não encontrado"),
 *      @SWG\ResponseMessage(code=500, message="Erro interno no servidor")
 *   )
 * )
 */
 // Listar
$app->get('/admin/restaurantes', function () use ($app, $restaurantes) {
    $res = $restaurantes->Query("SELECT RestauranteID AS 'ID', res_nome AS 'Nome', res_ativo AS 'Ativo'
									FROM tbl_restaurantes
									WHERE res_status = 1");

    $colunas = array_keys($res->res[0]);

    $app->render('admin/restaurantes.html.twig', array('linhas'=>$res->res, 'colunas'=>$colunas, 'pagina'=>'Restaurantes', 'rota'=>'restaurantes'));
});

/**
 *
 * @SWG\Api(
 *   path="/admin/restaurantes/{RestauranteID}",
 *   description="Listar Restaurante",
 *   @SWG\Operation(method="POST", summary="Listar Restaurante", notes="", type="void", nickname="listarestaurantes",
 *      @SWG\Parameters(
 *          @SWG\Parameter(
 *              name="RestauranteID",
 *              description="ID no banco de dados (RestauranteID)",
 *              paramType="path",
 *              required=true,
 *              type="string"
 *          ),
 *      ),
 *      @SWG\ResponseMessage(code=200, message="Sucesso"),
 *      @SWG\ResponseMessage(code=404, message="Não encontrado"),
 *      @SWG\ResponseMessage(code=500, message="Erro interno no servidor")
 *   )
 * )
 */
// Dados
$app->get('/admin/restaurantes/:id', function ($id) use ($app, $restaurantes) {
    $R = $restaurantes->Query("SELECT *
								FROM tbl_restaurantes
								WHERE res_status = 1 AND RestauranteID = " . $id, 'unica');

	echo json_encode($R, JSON_NUMERIC_CHECK);
});

/**
 *
 * @SWG\Api(
 *   path="/admin/restaurantes/{RestauranteID}",
 *   description="Deletar Restaurante",
 *   @SWG\Operation(method="POST", summary="Deletar Restaurante", notes="", type="void", nickname="deleterestaurantes",
 *      @SWG\Parameters(
 *          @SWG\Parameter(
 *              name="RestauranteID",
 *              description="ID no banco de dados (RestauranteID)",
 *              paramType="path",
 *              required=true,
 *              type="string"
 *          ),
 *      ),
 *      @SWG\ResponseMessage(code=200, message="Sucesso"),
 *      @SWG\ResponseMessage(code=404, message="Não encontrado"),
 *      @SWG\ResponseMessage(code=500, message="Erro interno no servidor")
 *   )
 * )
 */
// Delete
$app->delete('/admin/restaurantes/:id', function ($id) use ($app, $restaurantes) {
    $R = $restaurantes->delete('RestauranteID = ' . $id);

	echo json_encode($R, JSON_NUMERIC_CHECK);
});

/**
 *
 * @SWG\Api(
 *   path="/admin/restaurantes/imagem/{ArquivoID}",
 *   description="Imagem do Restaurante",
 *   @SWG\Operation(method="POST", summary="Imagem do Restaurante", notes="", type="void", nickname="imagemrestaurantes",
 *      @SWG\Parameters(
 *          @SWG\Parameter(
 *              name="ArquivoID",
 *              description="ID no banco de dados (ArquivoID)",
 *              paramType="path",
 *              required=true,
 *              type="string"
 *          ),
 *      ),
 *      @SWG\ResponseMessage(code=200, message="Sucesso"),
 *      @SWG\ResponseMessage(code=404, message="Não encontrado"),
 *      @SWG\ResponseMessage(code=500, message="Erro interno no servidor")
 *   )
 * )
 */
// Imagem
$app->get('/admin/restaurantes/imagem/:id', function ($id) use ($app, $restaurantes) {
    $R = $restaurantes->Query("SELECT CONCAT('" . (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['SERVER_NAME'] . "/service/web/uploads/', arq_extensao, '/', arq_nome) AS arq_nome, arq_extensao
								FROM tbl_arquivos
								WHERE ArquivoID = " . $id, "unica");

	echo json_encode($R->res, JSON_NUMERIC_CHECK);
});
