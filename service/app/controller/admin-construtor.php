<?php
use app\model\Construtor;

$construtor  = new Construtor(array(), $app->db);

$app->get('/admin/construtor', function () use ($app, $construtor) {
    $app->render('admin/construtor.html.twig', array('pagina'=>'Construtor', 'rota'=>'construtor'));

});


$app->post('/admin/construtor', function () use ($app, $construtor) {
	
	if(sizeof($app->request->post('campos')) == 0) {
		$R->cod = 500;
		$R->res = "Informe pelo menos um campo";
		
		echo json_encode($R, JSON_NUMERIC_CHECK);
		exit;
	}
	
	
	$campos = $app->request->post();
	
	$dados['templateNome']   = ucwords(strtolower($campos['const_nome']));
	$dados['controllerNome'] = str_replace(" ", "", ucwords(strtolower($campos['const_nome'])));
	$dados['modelNome'] 	 = str_replace(" ", "", ucwords(strtolower($campos['const_nome'])));
	$dados['tabelaNome']     = 'tbl_' . str_replace("-", "_", $app->slug->slugify($campos['const_nome']));
	$dados['tabelaPrefixo']  = substr($campos['campos'][0]['const_name'], 0, 3);	
	$dados['tabelaId']       = str_replace(" ", "", ucwords(strtolower($campos['const_nome']))) . "ID";
	$dados['tabelaStatus']   = $dados['tabelaPrefixo'] . '_status';
	$dados['controllerRota'] = ($campos['const_rota'] == "" ? $app->slug->slugify($campos['const_nome']) : strtolower($campos['const_rota']));
	
	// Valida se existe tabela com o mesmo nome.
	$T = $construtor->Query("SHOW TABLES FROM " . $app->database . " LIKE '" . $dados['tabelaNome'] . "'");
	if($T->cod == 200) {
		$R->cod = 500;
		$R->res = "Já existe uma tabela com o nome " . $dados['tabelaNome'];
		
		echo json_encode($R, JSON_NUMERIC_CHECK);
		exit;
	}
	
	// Variaveis da Documentação, formulário e tabela no banco de dados
	$campoForm = "";
	$sql	   = "CREATE TABLE " . $dados['tabelaNome'] . " (" . $dados['tabelaId'] . " INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,";

	$dados['docComentarioEditar'] = ' *    @SWG\Parameter(name="' . $dados['tabelaId'] . '", description="", paramType="form", required="true", type="string"),';
	for($i = 0; $i < sizeof($campos['campos']); $i++) {
		$dados['docComentario'] .= ' *    @SWG\Parameter(name="' . $campos['campos'][$i]['const_name'] . '", description="", paramType="form", required=' . ($campos['campos'][$i]['const_required'] == 1 ? "true" : "false") . ', type="string"),';
		
		$maxlength = "";
		if($campos['campos'][$i]['const_size'] != '' && $campos['campos'][$i]['const_size'] > 0) {
			$maxlength = " maxlength = '" . $campos['campos'][$i]['const_size'] . "'";
		}
		
		// Tipo de campos para formulário no template
		switch ($campos['campos'][$i]['const_type']) {
			case 'text':
				$campoForm .= "\t\t\t\t\t\t<div class='form-group'>\n";
				$campoForm .= "\t\t\t\t\t\t\t<label>" . $campos['campos'][$i]['const_label'] . ":</label>\n";
				$campoForm .= "\t\t\t\t\t\t\t<input class='form-control' type='text' name='" . $campos['campos'][$i]['const_name'] . "' id='" . $campos['campos'][$i]['const_name'] . "' value='" . $campos['campos'][$i]['const_value'] . "' " . ($campos['campos'][$i]['const_required'] == 1 ? 'required' : '') . $maxlength . ">";
				$campoForm .= "\t\t\t\t\t\t</div>\n";
				
				$sql .= $campos['campos'][$i]['const_name'] . " VARCHAR(" . $campos['campos'][$i]['const_size'] . "),";
				break;

			case 'textarea':
				$campoForm .= "<div class='form-group'>\n";
				$campoForm .= "\t\t\t\t\t\t\t\t<label>" . $campos['campos'][$i]['const_label'] . ":</label>\n";
				$campoForm .= "\t\t\t\t\t\t\t\t<textarea class='form-control' name='" . $campos['campos'][$i]['const_name'] . "' id='" . $campos['campos'][$i]['const_name'] . "' " . ($campos['campos'][$i]['const_required'] == 1 ? 'required' : '') . ">" . $campos['campos'][$i]['const_value'] . "</textarea>\n";
				$campoForm .= "\t\t\t\t\t\t\t</div>\n";
				
				$sql .= $campos['campos'][$i]['const_name'] . " TEXT,";
				break;
				
			case 'password':
				$campoForm .= "\t\t\t\t\t\t\t<div class='form-group'>\n";
				$campoForm .= "\t\t\t\t\t\t\t\t<label>" . $campos['campos'][$i]['const_label'] . ":</label>\n";
				$campoForm .= "\t\t\t\t\t\t\t\t<input class='form-control' type='password' name='" . $campos['campos'][$i]['const_name'] . "' id='" . $campos['campos'][$i]['const_name'] . "' value='" . $campos['campos'][$i]['const_value'] . "' " . ($campos['campos'][$i]['const_required'] == 1 ? 'required' : '') . $maxlength . ">\n";
				$campoForm .= "\t\t\t\t\t\t\t</div>\n";
				
				$sql .= $campos['campos'][$i]['const_name'] . " VARCHAR(40) NOT NULL,";
				break;

			case 'email':
				$campoForm .= "\t\t\t\t\t\t\t<div class='form-group'>\n";
				$campoForm .= "\t\t\t\t\t\t\t\t<label>" . $campos['campos'][$i]['const_label'] . ":</label>\n";
				$campoForm .= "\t\t\t\t\t\t\t\t<input class='form-control' type='email' name='" . $campos['campos'][$i]['const_name'] . "' id='" . $campos['campos'][$i]['const_name'] . "' value='" . $campos['campos'][$i]['const_value'] . "' " . ($campos['campos'][$i]['const_required'] == 1 ? 'required' : '') . $maxlength . ">\n";
				$campoForm .= "\t\t\t\t\t\t\t</div>\n";
				
				$sql .= $campos['campos'][$i]['const_name'] . " VARCHAR(" . $campos['campos'][$i]['const_size'] . "),";
				break;

			case 'select':
				$campoForm .= "\t\t\t\t\t\t\t\t<div class='form-group'>\n";
				$campoForm .= "\t\t\t\t\t\t\t\t\t<label>" . $campos['campos'][$i]['const_label'] . ":</label>\n";
				$campoForm .= "\t\t\t\t\t\t\t\t\t<select class='form-control' name='" . $campos['campos'][$i]['const_name'] . "' id='" . $campos['campos'][$i]['const_name'] . "' " . ($campos['campos'][$i]['const_required'] == 1 ? 'required' : '') . ">\n";
				
				$option = $campos['campos'][$i]['const_options'];
				if($option && strlen($option) > 0) {
					$opt = explode(",", $option);
				
					for($o = 0; $o < sizeof($opt); $o++) {
						$campoForm .= "\t\t\t\t\t\t\t\t\t\t<option value='" . ($o + 1) . "'>" . $opt[$o] . "</option>\n";
					}
				}	
				
				$campoForm .= "\t\t\t\t\t\t\t\t\t</select>\n";
				$campoForm .= "\t\t\t\t\t\t\t\t</div>\n";
				
				$sql .= $campos['campos'][$i]['const_name'] . " INT(11) UNSIGNED,";
				break;
				
			case 'radio':
				$campoForm .= "\t\t\t\t\t\t\t\t<div class='form-group'>\n";
				$campoForm .= "\t\t\t\t\t\t\t\t\t<label>" . $campos['campos'][$i]['const_label'] . ":</label>\n";

				$option = $campos['campos'][$i]['const_options'];
				if($option && strlen($option) > 0) {
					$opt = explode(",", $option);
				
					for($o = 0; $o < sizeof($opt); $o++) {
						$campoForm .= "\t\t\t\t\t\t\t\t\t<input class='form-control' type='radio' name='" . $campos['campos'][$i]['const_name'] . "' id='" . $campos['campos'][$i]['const_name'] . ($o + 1) . "' value='" . trim($opt[$o]) . "' " . ($campos['campos'][$i]['const_required'] == 1 ? 'required' : '') . "> " . trim($opt[$o]) . "\n";
					}
				}
				
				$campoForm .= "\t\t\t\t\t\t\t\t</div>\n";
				
				$sql .= $campos['campos'][$i]['const_name'] . " TEXT,";
				break;				

			case 'checkbox':
				$campoForm .= "\t\t\t\t\t\t\t\t<div class='form-group'>\n";
				$campoForm .= "\t\t\t\t\t\t\t\t\t<label>" . $campos['campos'][$i]['const_label'] . ":</label>\n";

				$option = $campos['campos'][$i]['const_options'];
				if($option && strlen($option) > 0) {
					$opt = explode(",", $option);
				
					for($o = 0; $o < sizeof($opt); $o++) {
						$campoForm .= "\t\t\t\t\t\t\t\t\t<input class='form-control' type='checkbox' name='" . $campos['campos'][$i]['const_name'] . "' id='" . $campos['campos'][$i]['const_name'] . ($o + 1) . "' value='" . trim($opt[$o]) . "' " . ($campos['campos'][$i]['const_required'] == 1 ? 'required' : '') . "> " . trim($opt[$o]) . "\n";
					}
				}
				
				$campoForm .= "\t\t\t\t\t\t\t\t</div>\n";
				
				$sql .= $campos['campos'][$i]['const_name'] . " TEXT,";
				break;								

			case 'file':
				$campoForm .= "\t\t\t\t\t\t\t\t<div class='form-group'>\n";
				$campoForm .= "\t\t\t\t\t\t\t\t\t<label>" . $campos['campos'][$i]['const_label'] . ":</label>\n";
				$campoForm .= "\t\t\t\t\t\t\t\t\t<input type='hidden' name='" . $campos['campos'][$i]['const_name'] . "' id='" . $campos['campos'][$i]['const_name'] . "' value='0'>\n";
				$campoForm .= "\t\t\t\t\t\t\t\t\t<input type='file' id='" . $campos['campos'][$i]['const_name'] . "_novo' name='" . $campos['campos'][$i]['const_name'] . "_novo'>\n";
				$campoForm .= "\t\t\t\t\t\t\t\t\t<br>\n";
				$campoForm .= "\t\t\t\t\t\t\t\t\t<div id='imagem_view'></div>\n";
				$campoForm .= "\t\t\t\t\t\t\t\t</div>\n";
						  
				$jsUploadify  = "<!-- UPLOADIFY -->\n";
				$jsUploadify .= "<script src='{{ assetUri }}/web/js/plugins/uploadify/jquery.uploadifive.js'></script>\n";
				$jsUploadify .= "<link href='{{ assetUri }}/web/css/plugins/uploadify/uploadifive.css' rel='stylesheet'>\n";
				$jsUploadify .= "<script type='text/javascript'>\n";
				$jsUploadify .= "\t$(function() {\n";
				$jsUploadify .= "\t\t// Novo\n";
				$jsUploadify .= "\t\t$('#" . $dados['tabelaPrefixo'] . "_imagem_novo').uploadifive({\n";
				$jsUploadify .= "\t\t\t\t'auto'             : true,\n";
				$jsUploadify .= "\t\t\t\t'queueSizeLimit'   : 1,\n";
				$jsUploadify .= "\t\t\t\t'multi'        	: false,\n";
				$jsUploadify .= "\t\t\t\t'removeCompleted'  : true,\n";
				$jsUploadify .= "\t\t\t\t'buttonText'   	: 'Selecionar imagem',\n";
				$jsUploadify .= "\t\t\t\t'uploadScript'     : '/service/admin/uploads',\n";
				$jsUploadify .= "\t\t\t\t'onUploadComplete' : function(file, data) {\n";
				$jsUploadify .= "\t\t\t\t\tvar dados = $.parseJSON(data);\n";
				$jsUploadify .= "\t\t\t\t\tif(dados.cod == 200) {\n";
				$jsUploadify .= "\t\t\t\t\t\t$('#form').find('#".$dados['tabelaPrefixo']."_imagem').val(dados.ArquivoID);\n";
				$jsUploadify .= "\t\t\t\t\t\t$('#form').find('#imagem_view').html('<img src=' + dados.arq_nome + ' width=\"280\">');\n";
				$jsUploadify .= "\t\t\t\t\t} else {\n";
				$jsUploadify .= "\t\t\t\t\t\talert('Falha no envio da imagem.');\n";
				$jsUploadify .= "\t\t\t\t\t}\n";
				$jsUploadify .= "\t\t\t\t}\n";
				$jsUploadify .= "\t\t\t\t});\n";
				$jsUploadify .= "\n";
				$jsUploadify .= "\t\t// Editar\n";
				$jsUploadify .= "\t\t$('#" . $dados['tabelaPrefixo'] . "_imagem_editar').uploadifive({\n";
				$jsUploadify .= "\t\t\t\t'auto'             : true,\n";
				$jsUploadify .= "\t\t\t\t'queueSizeLimit'   : 1,\n";
				$jsUploadify .= "\t\t\t\t'multi'        	: false,\n";
				$jsUploadify .= "\t\t\t\t'removeCompleted'  : true,\n";
				$jsUploadify .= "\t\t\t\t'buttonText'   	: 'Selecionar imagem',\n";
				$jsUploadify .= "\t\t\t\t'uploadScript'     : '/service/admin/uploads',\n";
				$jsUploadify .= "\t\t\t\t'onUploadComplete' : function(file, data) {\n";
				$jsUploadify .= "\t\t\t\t\tvar dados = $.parseJSON(data);\n";
				$jsUploadify .= "\t\t\t\tif(dados.cod == 200) {\n";
				$jsUploadify .= "\t\t\t\t\t$('#form-editar').find('#".$dados['tabelaPrefixo']."_imagem').val(dados.ArquivoID);\n";
				$jsUploadify .= "\t\t\t\t\t$('#form-editar').find('#imagem_view').html('<img src=' + dados.arq_nome + ' width=\"280\">');\n";
				$jsUploadify .= "\t\t\t\t} else {\n";
				$jsUploadify .= "\t\t\t\t\talert('Falha no envio da imagem.');\n";
				$jsUploadify .= "\t\t\t\t}\n";
				$jsUploadify .= "\t\t\t}\n";
				$jsUploadify .= "\t\t});\n";
				$jsUploadify .= "\t});\n";
				$jsUploadify .= "</script>\n";
				
				$sql .= $campos['campos'][$i]['const_name'] . " INT(11) UNSIGNED,";
				break;

			case 'hidden':
				$campoForm .= "<div class='form-group'>";
				$campoForm .= "<label>" . $campos['campos'][$i]['const_label'] . ":</label>";
				$campoForm .= "<input type='hidden' name='" . $campos['campos'][$i]['const_name'] . "' id='" . $campos['campos'][$i]['const_name'] . "' value='" . $campos['campos'][$i]['const_value'] . "'>";
				$campoForm .= "</div>\n";
				break;
				
		}
		//$campoForm .= '<div class="hr-line-dashed"></div>';

	}
	
	$sql .= $dados['tabelaPrefixo'] . "_slug VARCHAR(500) DEFAULT NULL,".
			$dados['tabelaPrefixo'] . "_data TIMESTAMP DEFAULT CURRENT_TIMESTAMP,".
			$dados['tabelaStatus'] . " TINYINT(1) NOT NULL DEFAULT '1'
			) ENGINE=InnoDB DEFAULT CHARSET=utf8";

	/**
	 * TABELA no BD
	 */
	$construtor->Query($sql);
							
	/**
	 * MODEL
	 */
	//Substitui variaveis e cria arquivo MODEL
    $conteudo = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/service/app/config/admin_base_model.php');
    $conteudo = str_replace(array('{NOME}', '{TABELA}', '{ID}', '{STATUS}'), array($dados['modelNome'], $dados['tabelaNome'], $dados['tabelaId'], $dados['tabelaStatus']), $conteudo);
	file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/service/app/model/' . $dados['modelNome'] . '.php', $conteudo);
	
	/**
	 * FORMULARIO
	 */
	// Formulario de novo registro
	$form  = "<form role='form' id='form' enctype='multipart/form-data'>\n";
	$form .= $campoForm;
	$form .= "<div>\n";
	$form .= "\t<button class='btn btn-w-m btn-default pull-left' type='button' data-dismiss='modal'><strong>Cancelar</strong></button>\n";
	$form .= "\t<button class='btn btn-w-m btn-primary pull-right' type='submit' id='salvarNovo'><strong>Salvar</strong></button>\n";
	$form .= "</div>\n";
	$form .= "</form>\n";
	
	// Formulario de edicao
	$formEditar  = "<form role='form' id='form-editar' enctype='multipart/form-data'>\n";
	$formEditar .= "\t\t\t\t\t\t\t<input type='hidden' name='" . $dados['tabelaId'] . "' id='" . $dados['tabelaId'] . "' value='0'>\n";
	$formEditar .= "\t\t\t\t\t\t\t" . str_replace("_novo", "_editar", $campoForm);
	$formEditar .= "\t\t\t\t\t\t\t\t<div>\n";
	$formEditar .= "\t\t\t\t\t\t\t\t\t<button class='btn btn-w-m btn-default pull-left' type='button' data-dismiss='modal'><strong>Cancelar</strong></button>\n";
	$formEditar .= "\t\t\t\t\t\t\t\t\t<button class='btn btn-w-m btn-primary pull-right' type='submit' id='salvarEditar'><strong>Salvar</strong></button>\n";
	$formEditar .= "\t\t\t\t\t\t\t\t</div>\n";
	$formEditar .= "\t\t\t\t\t\t</form>\n";

	// Substitui variáveis e cria arquivo FORMULARIO
    $conteudo = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/service/app/config/admin_base_formulario.html.twig');
    $conteudo = str_replace(array('{FORM}', '{FORMEDITAR}', '{UPLOADIFY}'), array($form, $formEditar, $jsUploadify), $conteudo);
	file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/service/templates/admin/' . $dados['controllerRota'] . '.html.twig', $conteudo);
	
	// Substitui variáveis e cria arquivo MENU
    $conteudo = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/service/app/config/admin_base_menu.html.twig');
    $conteudo = str_replace(array('{NOME}', '{ROTA}'), array($dados['templateNome'], $dados['controllerRota']), $conteudo);
	file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/service/templates/admin/base/menu.html.twig', $conteudo);
	
	/**
	 * CONTROLLER ADMIN
	 */
	// Concatenando ID + campos para a documentação
	$dados['docComentarioEditar'] = $dados['docComentarioEditar'] . $dados['docComentario'];
	// Substitui variáveis e cria arquivo CONTROLLER
    $conteudo = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/service/app/config/admin_base_controller.php');
    $conteudo = str_replace(array('{NOME}', '{NOMESLUG}', '{ROTA}', '{PREFIXO}', '{DOCUMENTACAO}', '{DOCUMENTACAO_EDICAO}', '{ID}', '{TABELA}', '{STATUS}'), array($dados['controllerNome'], '$' . $app->slug->slugify($campos['const_nome']), $dados['controllerRota'], $dados['tabelaPrefixo'], $dados['docComentario'], $dados['docComentarioEditar'], $dados['tabelaId'], $dados['tabelaNome'], $dados['tabelaStatus']), $conteudo);
	file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/service/app/controller/admin-' . $dados['controllerRota'] . '.php', $conteudo);
	
	/**
	 * CONTROLLER FRONT
	 */
    $conteudo = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/service/app/config/admin_base_controller_front.php');
    $conteudo = str_replace(array('{NOME}', '{NOMESLUG}', '{ROTA}', '{PREFIXO}', '{DOCUMENTACAO}', '{TABELA}', '{STATUS}'), array($dados['controllerNome'], '$' . $app->slug->slugify($campos['const_nome']), $dados['controllerRota'], $dados['tabelaPrefixo'], $dados['docComentario'], $dados['tabelaNome'], $dados['tabelaStatus']), $conteudo);
	file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/service/app/controller/' . $dados['controllerRota'] . '.php', $conteudo);
	
	// ***
	// Salva rota e modifica permissão do grupo admin
	// ***
	$R = $construtor->Query("SELECT * FROM tbl_rotas WHERE rot_nome = '" . $dados['controllerRota'] . "'");
	if($R->cod = 404) {
		$R = $construtor->Query("INSERT INTO tbl_rotas 
								(rot_nome, rot_status)
									VALUES 
								('/admin/" . $dados['controllerRota'] . "', 1)");

		// Atualiza grupo de acesso do admin
		$construtor->Query("UPDATE tbl_usuarios_grupo SET gru_rotas = CONCAT(gru_rotas, ',', " . $R->id . ")");
		
		$R = $construtor->Query("INSERT INTO tbl_rotas 
								(rot_nome, rot_status) 
									VALUES 
								('/admin/" . $dados['controllerRota'] . "/imagem', 1)");

		// Atualiza grupo de acesso do admin
		$construtor->Query("UPDATE tbl_usuarios_grupo SET gru_rotas = CONCAT(gru_rotas, ',', " . $R->id . ")");		
	}

    $R->cod = 200;

    echo json_encode($R, JSON_NUMERIC_CHECK);
});