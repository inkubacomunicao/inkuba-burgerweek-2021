<?php
use app\model\Restaurantes;
$restaurantes = new Restaurantes(array(), $app->db);

$app->get('/filtros(/:uf)', function ($uf = '') use ($app, $restaurantes) {
	header('Content-Type: application/json');

	$R->ufs = $restaurantes->Query("SELECT DISTINCT(res_estado) AS uf
								FROM tbl_restaurantes
								WHERE res_status = 1 AND res_ativo = 1
								GROUP BY res_estado
								ORDER BY res_estado ASC")->res;

	if (strlen($uf) == 0) {
		$R->cidades = $restaurantes->Query("SELECT DISTINCT(res_cidade) AS cidade
								FROM tbl_restaurantes
								WHERE res_status = 1 AND res_ativo = 1
								GROUP BY res_cidade
								ORDER BY res_cidade ASC")->res;
	} else {

		$R->cidades = $restaurantes->Query("SELECT DISTINCT(res_cidade) AS cidade
									FROM tbl_restaurantes
									WHERE res_status = 1 AND res_ativo = 1 AND res_estado = '" . $uf . "'
									GROUP BY res_cidade
									ORDER BY res_cidade ASC")->res;
	}

    echo json_encode($R, JSON_NUMERIC_CHECK);
});
