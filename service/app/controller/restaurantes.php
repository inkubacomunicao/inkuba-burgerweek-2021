<?php
use app\model\Restaurantes;

$restaurantes = new Restaurantes(array(), $app->db);

$app->get('/restaurantes/:pagina/:qtdPorPagina', function ($pagina, $qtdPorPagina) use ($app, $restaurantes) {
	header('Content-Type: application/json');

	if($pagina == 1) {
		$paginacao = 0;
	} else {
		$paginacao = ($pagina * $qtdPorPagina) - $qtdPorPagina;
	}

	$filtro = $app->request->get();

	if(!isset($filtro['raio'])) {
		$filtro['raio'] = 1000000; //km
	}

	$strInner = "";
	$strWhere = "";
    if($filtro['nome']) {
        $strWhere .= " res_nome LIKE '%" . $filtro['nome'] . "%' AND ";
	}

	if($filtro['uf']) {

		if($filtro['uf'] == 'BH') {
        	$strWhere .= " res_estado = 'MG' AND ";
		} else {
			$strWhere .= " res_estado = '" . $filtro['uf'] . "' AND ";
		}

	}

	if($filtro['cidade']) {
		$strWhere .= " res_cidade = '" . $filtro['cidade'] . "' AND ";

	}

	if($filtro['favoritos']) {
		$strWhere .= " RestauranteID IN (" . $filtro['favoritos'] . ") AND ";
	}

	$S = "SELECT SQL_CALC_FOUND_ROWS *,
			(SELECT CONCAT('" . (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['SERVER_NAME'] . "/service/web/uploads/', arq_extensao, '/', arq_nome) FROM tbl_arquivos WHERE ArquivoID = res_imagem AND arq_status = 1) AS res_imagem
			FROM tbl_restaurantes
			" . $strInner . "
			WHERE " . $strWhere . " res_status = 1 AND res_ativo = 1
			ORDER BY res_nome ASC
			LIMIT " . $paginacao . ", " . $qtdPorPagina;

	if($filtro['lat'] && $filtro['lon']) {
		$S = "SELECT SQL_CALC_FOUND_ROWS *,
				(SELECT CONCAT('" . (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['SERVER_NAME'] . "/service/web/uploads/', arq_extensao, '/', arq_nome) FROM tbl_arquivos WHERE ArquivoID = res_imagem AND arq_status = 1) AS res_imagem,
				ACOS(SIN(RADIANS(res_latitude)) * SIN(RADIANS(" . $filtro['lat'] . ")) + COS(RADIANS( res_latitude ))
				* COS(RADIANS(" . $filtro['lat'] . ")) * COS(RADIANS(res_longitude) - RADIANS(" . $filtro['lon'] . "))) * 6380 AS res_distancia
				FROM tbl_restaurantes
				" . $strInner . "
				WHERE " . $strWhere . " res_status = 1 AND
				ACOS(SIN(RADIANS(res_latitude)) * SIN(RADIANS(" . $filtro['lat'] . ")) + COS(RADIANS(res_latitude))
				* COS(RADIANS(" . $filtro['lat'] . ")) * COS(RADIANS(res_longitude) - RADIANS(" . $filtro['lon'] . "))) * 6380 < " . $filtro['raio'] . "
				ORDER BY res_distancia LIMIT " . $paginacao . ", " . $qtdPorPagina;

	}

  //   if($filtro['cep']) {
	// 	$postalCode = str_ireplace(" ","%20", $filtro['cep']);
	// 	$output = file_get_contents("http://dev.virtualearth.net/REST/v1/Locations/BR/SP/" . $postalCode . "/Sao%20Paulo/-?output=json&key=AmKdDR9ZCGccBtMw79Ui4zT5Qjq7HMezqI19upmXbeYQNhTN1sJi2Q6pUz3NvU2k");
	// 	$array = json_decode($output, true);
	//
	// 	if($array['statusCode'] == 200) {
	// 		$filtro['latitude'] = str_replace(',', '.', $array['resourceSets'][0]['resources'][0]['point']['coordinates'][0]); // Latitude
	// 		$filtro['longitude'] = str_replace(',', '.', $array['resourceSets'][0]['resources'][0]['point']['coordinates'][1]); // Longitude
	//
	// 		$S = "SELECT SQL_CALC_FOUND_ROWS *,
	// 				(SELECT CONCAT('" . (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['SERVER_NAME'] . "/service/web/uploads/', arq_extensao, '/', arq_nome) FROM tbl_arquivos WHERE ArquivoID = res_imagem AND arq_status = 1) AS res_imagem,
	// 				ACOS(SIN(RADIANS(res_latitude)) * SIN(RADIANS(" . $filtro['latitude'] . ")) + COS(RADIANS( res_latitude ))
	// 				* COS(RADIANS(" . $filtro['latitude'] . ")) * COS(RADIANS(res_longitude) - RADIANS(" . $filtro['longitude'] . "))) * 6380 AS res_distancia
	// 				FROM tbl_restaurantes
	// 				" . $strInner . "
	// 				WHERE " . $strWhere . " res_status = 1 AND
	// 				ACOS(SIN(RADIANS(res_latitude)) * SIN(RADIANS(" . $filtro['latitude'] . ")) + COS(RADIANS(res_latitude))
	// 				* COS(RADIANS(" . $filtro['latitude'] . ")) * COS(RADIANS(res_longitude) - RADIANS(" . $filtro['longitude'] . "))) * 6380 < " . $filtro['raio'] . "
	// 				ORDER BY res_distancia LIMIT " . $paginacao . ", " . $qtdPorPagina;
	//
	// 	    $R->rua = $array['resourceSets'][0]['resources'][0]['name'];
	//
	// 	} else {
	// 		$R->cod = 404;
	// 		$R->msg = "Não foi possível pesquisar pelo CEP";
	// 	}
	// }

	$R = $restaurantes->Query($S);

	$R->total = $restaurantes->Query('SELECT FOUND_ROWS() AS totalFound')->res[0]['totalFound'];

	for ($i=0; $i < $R->qtd; $i++) {
		// Galeria
		if(strlen($R->res[$i]['res_galeria']) > 0) {
			$R->res[$i]['res_galeria'] = $restaurantes->Query("SELECT CONCAT('" . (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['SERVER_NAME'] . "/service/web/uploads/', arq_extensao, '/', arq_nome) AS imagem
																FROM tbl_restaurantes
																INNER JOIN tbl_arquivos ON FIND_IN_SET(ArquivoID, res_galeria)
																WHERE arq_status = 1 AND RestauranteID = " . $R->res[$i]['RestauranteID'])->res;
		} else {
			$R->res[$i]['res_galeria'] = 0;
		}

		$R->res[$i]['res_cardapio'] = $restaurantes->Query("SELECT *
															FROM tbl_restaurantes_cardapios
															WHERE car_status = 1 AND car_RestauranteID = " . $R->res[$i]['RestauranteID'])->res;

		// for ($c=0; $c < sizeof($R->res[$i]['res_cardapio']); $c++) {
		// 	$R->res[$i]['res_cardapio'][$c]['car_galeria'] = $restaurantes->Query("SELECT CONCAT('" . (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['SERVER_NAME'] . "/service/web/uploads/', arq_extensao, '/', arq_nome) AS galeria
		// 																			FROM tbl_restaurantes_cardapios
		// 																			INNER JOIN tbl_arquivos ON FIND_IN_SET(ArquivoID, car_galeria)
		// 																			WHERE arq_status = 1 AND CardapioID = " . $R->res[$i]['res_cardapio'][$c]['CardapioID'])->res;
		// }
		//
		// if($R->res[$i]['res_patrocinadores'] != '') {
		// 	$R->res[$i]['res_patrocinadores'] = $restaurantes->Query("SELECT PatrocinadorID, pat_nome, pat_slug FROM tbl_patrocinadores WHERE pat_status = 1 AND PatrocinadorID IN(" . $R->res[$i]['res_patrocinadores'] . ")")->res;
		// } else {
		// 	$R->res[$i]['res_patrocinadores'] = array();
		// }
		// $R->res[$i]['res_TipoPedido'] = explode(",", $R->res[$i]['res_TipoPedido']);

	}

    echo json_encode($R, JSON_NUMERIC_CHECK);
});





$app->get('/restaurante-detalhes/:slug', function ($slug) use ($app, $restaurantes) {
	header('Content-Type: application/json');

	$R = $restaurantes->Query("SELECT *,
								CONCAT('" . (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['SERVER_NAME'] . "/restaurante/', res_slug) AS res_slug,
								(SELECT CONCAT('" . (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['SERVER_NAME'] . "/service/web/uploads/', arq_extensao, '/', arq_nome) FROM tbl_arquivos WHERE ArquivoID = res_imagem AND arq_status = 1) AS res_imagem,
								(SELECT cat_nome FROM tbl_restaurantes_categorias WHERE CategoriaID = res_CategoriaID) AS res_CategoriaID,
								(SELECT tib_nome FROM tbl_restaurantes_tipos_burgers WHERE TipoBurgerID = res_TipoBurgerID) AS res_TipoBurgerID
								FROM tbl_restaurantes
								WHERE res_status = 1 AND res_ativo = 1 AND res_slug = '" . $slug . "'");

	for ($i=0; $i < $R->qtd; $i++) {
		// Galeria
		$R->res[$i]['res_galeria'] = $restaurantes->Query("SELECT CONCAT('" . (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['SERVER_NAME'] . "/service/web/uploads/', arq_extensao, '/', arq_nome) AS imagem
															FROM tbl_restaurantes
															INNER JOIN tbl_arquivos ON FIND_IN_SET(ArquivoID, res_galeria)
															WHERE arq_status = 1 AND RestauranteID = " . $R->res[$i]['RestauranteID'])->res;

		$R->res[$i]['res_cardapio'] = $restaurantes->Query("SELECT *
															FROM tbl_restaurantes_cardapios
															WHERE car_status = 1 AND car_RestauranteID = " . $R->res[$i]['RestauranteID'])->res;

		for ($c=0; $c < sizeof($R->res[$i]['res_cardapio']); $c++) {
			$R->res[$i]['res_cardapio'][$c]['car_galeria'] = $restaurantes->Query("SELECT CONCAT('" . (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['SERVER_NAME'] . "/service/web/uploads/', arq_extensao, '/', arq_nome) AS galeria
																					FROM tbl_restaurantes_cardapios
																					INNER JOIN tbl_arquivos ON FIND_IN_SET(ArquivoID, car_galeria)
																					WHERE arq_status = 1 AND CardapioID = " . $R->res[$i]['res_cardapio'][$c]['CardapioID'])->res;
		}

		if($R->res[$i]['res_patrocinadores'] != '') {
			$R->res[$i]['res_patrocinadores'] = $restaurantes->Query("SELECT PatrocinadorID, pat_nome, pat_slug FROM tbl_patrocinadores WHERE pat_status = 1 AND PatrocinadorID IN(" . $R->res[$i]['res_patrocinadores'] . ")")->res;
		} else {
			$R->res[$i]['res_patrocinadores'] = array();
		}
		$R->res[$i]['res_TipoPedido'] = explode(",", $R->res[$i]['res_TipoPedido']);

	}


	echo json_encode($R, JSON_NUMERIC_CHECK);
});
