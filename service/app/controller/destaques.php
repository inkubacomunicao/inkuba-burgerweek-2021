<?php
use app\model\DestaquesHome;

$destaques = new DestaquesHome(array(), $app->db);

$app->get('/destaques', function () use ($app, $destaques) {
	header('Content-Type: application/json');

	$R = $destaques->Query("SELECT *,
							(SELECT CONCAT('" . (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['SERVER_NAME'] . "/service/web/uploads/', arq_extensao, '/', arq_nome) FROM tbl_arquivos WHERE ArquivoID = deh_imagem AND arq_status = 1) AS deh_imagem,
							(SELECT CONCAT('" . (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['SERVER_NAME'] . "/service/web/uploads/', arq_extensao, '/', arq_nome) FROM tbl_arquivos WHERE ArquivoID = deh_imagem_mobile AND arq_status = 1) AS deh_imagem_mobile
							FROM tbl_destaques_home
							WHERE deh_status = 1 AND deh_ativo = 1
							ORDER BY deh_ordem ASC");

    echo json_encode($R, JSON_NUMERIC_CHECK);
});
