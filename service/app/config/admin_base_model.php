<?php

namespace app\model;

use lib\Db\Crud as Crud;

class {NOME} Extends Crud
{
    /**
     * @var string $table Nome da tabela
     */
    protected $table = '{TABELA}';
    
    /**
     * @var string $pk Chave primária da tabela
     */
    protected $pk    = '{ID}';
	
    /**
     * @var string $status Status do registro na tabela (0 = excluido)
     */	
	protected $status = '{STATUS}';
	
}
