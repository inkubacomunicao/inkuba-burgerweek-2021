<?php
/**
 * @package
 * @category
 * @subpackage
 *
 * @SWG\Resource(
 *   apiVersion="1.0.0",
 *   swaggerVersion="1.2",
 *   basePath="http://localhost/service",
 *   resourcePath="/{ROTA}",
 *   description="Operações de {NOME}",
 *   produces="['application/json']"
 * )
 */

use app\model\{NOME};
 
{NOMESLUG} = new {NOME}(array(), $app->db);

/**
 *
 * @SWG\Api(
 *   path="/{ROTA}",
 *   description="Listagem de {NOME}",
 *   @SWG\Operation(method="GET", summary="Listagem de {NOME}", type="string", nickname="lista{ROTA}",
 *      @SWG\Parameter(
 *          name="ativo",
 *          description="ativo",
 *          paramType="path",
 *          required=true,
 *          type="string"
 *      )
 *   )
 * )
 */
$app->get('/{ROTA}((/:pagina/:total)(/:slug))', function ($pagina, $total, $slug) use ($app, {NOMESLUG}) {
	header('Content-Type: application/json');
	
	if(gettype($slug) == 'string') {
		$R = {NOMESLUG}->Query("SELECT *, CONCAT('http://', '" . $_SERVER['SERVER_NAME'] . "/', 'service/web/uploads/', arq_extensao, '/', arq_nome) AS arq_nome
								FROM {TABELA}
								LEFT OUTER JOIN tbl_arquivos ON ArquivoID = {PREFIXO}_imagem
								WHERE arq_status = 1 AND {PREFIXO}_status = 1 AND {PREFIXO}_slug = '" . $slug . "'", 'unica');

	} else if(gettype($slug) == 'number') {
		$R = {NOMESLUG}->Query("SELECT *, CONCAT('http://', '" . $_SERVER['SERVER_NAME'] . "/', 'service/web/uploads/', arq_extensao, '/', arq_nome) AS arq_nome
								FROM {TABELA}
								LEFT OUTER JOIN tbl_arquivos ON ArquivoID = {PREFIXO}_imagem
								WHERE arq_status = 1 AND {PREFIXO}_status = 1 AND {ID} = " . $slug, 'unica');

	} else {
		$R = {NOMESLUG}->Query("SELECT *, CONCAT('http://', '" . $_SERVER['SERVER_NAME'] . "/', 'service/web/uploads/', arq_extensao, '/', arq_nome) AS arq_nome
								FROM {TABELA}
								LEFT OUTER JOIN tbl_arquivos ON ArquivoID = {PREFIXO}_imagem
								WHERE arq_status = 1 AND {PREFIXO}_status = 1
								LIMIT " . $pagina . "," . $total);
	
		$R->total = {NOMESLUG}->Query("SELECT COUNT(*)
										FROM {TABELA}
										WHERE arq_status = 1 AND {PREFIXO}_status = 1");
	
	}
	
    echo json_encode($R, JSON_NUMERIC_CHECK);
});
