<?php
/**
 * @package
 * @since  Tue, 29 Apr 14 16:45:02 -0300
 * @category
 * @subpackage
 *
 * @SWG\Resource(
 *   apiVersion="1.0.0",
 *   swaggerVersion="1.2",
 *   basePath="http://localhost/admin",
 *   resourcePath="/{ROTA}",
 *   description="Operações Admin. {NOME}",
 *   produces="['application/json']"
 * )
 */

use app\model\{NOME};

{NOMESLUG} = new {NOME}(array(), $app->db);

/**
 *
 * @SWG\Api(
 *   path="/admin/{ROTA}",
 *   description="Cadastrar {NOME}",
 *   @SWG\Operation(method="POST", summary="Cadastrar {NOME}", notes="", type="void", nickname="cadastra{ROTA}",
 *      @SWG\Parameters(
{DOCUMENTACAO}
 *       ),
 *      @SWG\ResponseMessage(code=200, message="Sucesso"),
 *      @SWG\ResponseMessage(code=404, message="Não encontrado"),
 *      @SWG\ResponseMessage(code=500, message="Erro interno no servidor") 
 *   )
 * )
 */
 // NOVO
$app->post('/admin/{ROTA}', function () use ($app, {NOMESLUG}) {
    $campos = $app->request->post();

	// slug do nome
	$campos['{PREFIXO}_slug'] = $app->slug->slugify($campos['{PREFIXO}_nome']);
	
    $R = {NOMESLUG}->create($campos);
	
	if($R->cod == 200) {
		$R->res = 'Dados salvos com sucesso. Redirecionando...';
	} else {
		$R->res = 'Falha ao salvar dados';
	}
	
	echo json_encode($R, JSON_NUMERIC_CHECK);
	
});


/**
 *
 * @SWG\Api(
 *   path="/admin/{ROTA}/{{ID}}",
 *   description="Editar {NOME}",
 *   @SWG\Operation(method="POST", summary="Editar {NOME}", notes="", type="void", nickname="edita{ROTA}",
 *      @SWG\Parameters(
{DOCUMENTACAO_EDICAO}
 *       ),
 *      @SWG\ResponseMessage(code=200, message="Sucesso"),
 *      @SWG\ResponseMessage(code=404, message="Não encontrado"),
 *      @SWG\ResponseMessage(code=500, message="Erro interno no servidor") 
 *   )
 * )
 */
 // EDITAR
$app->post('/admin/{ROTA}/:id', function ($id) use ($app, {NOMESLUG}) {
	$campos  = $app->request->post();
	
	// slug do nome
	$campos['{PREFIXO}_slug'] = $app->slug->slugify($campos['{PREFIXO}_nome']);
	
    $res = {NOMESLUG}->findById($id);

    if ($res->cod == 404) {
        $app->notFound();
        exit;
    }
	
	unset($campos['{ID}']);

	$R = {NOMESLUG}->save($campos, '{ID} = ' . $id);

	if($R->cod == 200) {
		$R->res = "Dados salvos com sucesso. Redirecionando...";
	} else {
		$R->res = "Houve um erro. Tente novamente.";
	}
	
	echo json_encode($R, JSON_NUMERIC_CHECK);

});

/**
 *
 * @SWG\Api(
 *   path="/admin/{ROTA}",
 *   description="Listar {NOME}",
 *   @SWG\Operation(method="POST", summary="Listar {NOME}", notes="", type="void", nickname="lista{ROTA}",
 *      @SWG\ResponseMessage(code=200, message="Sucesso"),
 *      @SWG\ResponseMessage(code=404, message="Não encontrado"),
 *      @SWG\ResponseMessage(code=500, message="Erro interno no servidor") 
 *   )
 * )
 */
 // Listar
$app->get('/admin/{ROTA}', function () use ($app, {NOMESLUG}) {
    $res = {NOMESLUG}->Query("SELECT {ID} AS 'ID', {PREFIXO}_nome AS 'Nome'
								FROM {TABELA}
								WHERE {STATUS} = 1");

    $colunas = array_keys($res->res[0]);
	
    $app->render('admin/{ROTA}.html.twig', array('linhas'=>$res->res, 'colunas'=>$colunas, 'pagina'=>'{NOME}', 'rota'=>'{ROTA}'));
});

/**
 *
 * @SWG\Api(
 *   path="/admin/{ROTA}/{{ID}}",
 *   description="Listar {NOME}",
 *   @SWG\Operation(method="POST", summary="Listar {NOME}", notes="", type="void", nickname="lista{ROTA}",
 *      @SWG\Parameters(
 *          @SWG\Parameter(
 *              name="{ID}",
 *              description="ID no banco de dados ({ID})",
 *              paramType="path",
 *              required=true,
 *              type="string"
 *          ),
 *      ), 
 *      @SWG\ResponseMessage(code=200, message="Sucesso"),
 *      @SWG\ResponseMessage(code=404, message="Não encontrado"),
 *      @SWG\ResponseMessage(code=500, message="Erro interno no servidor") 
 *   )
 * )
 */
// Dados
$app->get('/admin/{ROTA}/:id', function ($id) use ($app, {NOMESLUG}) {
    $R = {NOMESLUG}->Query("SELECT *
								FROM {TABELA}
								WHERE {PREFIXO}_status = 1 AND {ID} = " . $id, 'unica');

	echo json_encode($R, JSON_NUMERIC_CHECK);
});

/**
 *
 * @SWG\Api(
 *   path="/admin/{ROTA}/{{ID}}",
 *   description="Deletar {NOME}",
 *   @SWG\Operation(method="POST", summary="Deletar {NOME}", notes="", type="void", nickname="delete{ROTA}",
 *      @SWG\Parameters(
 *          @SWG\Parameter(
 *              name="{ID}",
 *              description="ID no banco de dados ({ID})",
 *              paramType="path",
 *              required=true,
 *              type="string"
 *          ),
 *      ), 
 *      @SWG\ResponseMessage(code=200, message="Sucesso"),
 *      @SWG\ResponseMessage(code=404, message="Não encontrado"),
 *      @SWG\ResponseMessage(code=500, message="Erro interno no servidor") 
 *   )
 * )
 */
// Delete
$app->delete('/admin/{ROTA}/:id', function ($id) use ($app, {NOMESLUG}) {
    $R = {NOMESLUG}->delete('{ID} = ' . $id);

	echo json_encode($R, JSON_NUMERIC_CHECK);
});

/**
 *
 * @SWG\Api(
 *   path="/admin/{ROTA}/imagem/{ArquivoID}",
 *   description="Imagem do {NOME}",
 *   @SWG\Operation(method="POST", summary="Imagem do {NOME}", notes="", type="void", nickname="imagem{ROTA}",
 *      @SWG\Parameters(
 *          @SWG\Parameter(
 *              name="ArquivoID",
 *              description="ID no banco de dados (ArquivoID)",
 *              paramType="path",
 *              required=true,
 *              type="string"
 *          ),
 *      ), 
 *      @SWG\ResponseMessage(code=200, message="Sucesso"),
 *      @SWG\ResponseMessage(code=404, message="Não encontrado"),
 *      @SWG\ResponseMessage(code=500, message="Erro interno no servidor") 
 *   )
 * )
 */
// Imagem
$app->get('/admin/{ROTA}/imagem/:id', function ($id) use ($app, {NOMESLUG}) {
    $R = {NOMESLUG}->Query("SELECT CONCAT('http://" . $_SERVER['SERVER_NAME'] . "/service/web/uploads/', arq_extensao, '/', arq_nome) AS arq_nome 
								FROM tbl_arquivos 
								WHERE ArquivoID = " . $id, "unica");

	echo json_encode($R->res, JSON_NUMERIC_CHECK);
});