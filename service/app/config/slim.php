<?php

$settings = array(
    'slim' => array(
        'templates.path'      => ROOT.'/templates/',
        'view'                => new \Slim\Views\Twig(),
        'debug'               => true,
        'cookies.encrypt'     => true,
        'cookies.secret_key'  => md5('appsecretkey'),
        'cookies.cipher'      => MCRYPT_RIJNDAEL_256,
        'cookies.cipher_mode' => MCRYPT_MODE_CBC,
        'log.enabled'         => true,
        'log.writer'          => new \lib\Log\DateTimeFileWriter(array(
                                    'path'           => ROOT.'/log',
                                    'name_format'    => 'Y-m-d',
                                    'message_format' => '%label% - %date% - %message%'
                                 )),
        'provider'            => 'PDO',
        'auth.type'           => 'sessao',
        'chave.expira'        => 86400,
        'login.url'           => '/admin/login',
        'secured.urls'        => array(
                                    array('path' => '/admin'),
                                    array('path' => '/admin/.+')
                                ),
        'upload.mimetypes'    => array(
                                    'image/jpeg',
                                    'image/jpg',
									'image/png',
									'video/mp4',
                                    'application/msword',
                                    'application/pdf'
                                ),
        'upload.max_size'     => '50M',
        'upload.path'         => ROOT.'/web/uploads/',
		'imagem.altura'		  => 1000,
		'imagem.largura'	  => 1000,
        'ip.servidor'         => array('127.0.0.1', 'fe80::1', '::1', 'cmsinkuba.localdev', '10.0.2.15', 'localhost'),
        'dominio.frontend'    => 'http://inkuba.com.br',
		'app.nome'			  => 'Inkuba',
    ),
    'session_cookies' => array(
        'expires' => '2 weeks',
    ),
    'database' => 'mysql',
    'mailer' => array(
       'host'     => 'smtp.sendgrid.net',
       'port'     => 587,
       'ssl'      => 'tls',
       'username' => 'inkuba',
       'password' => 'inkuba123',
    ),
    'facebook' => array(
        'appid'        => '',
        'secret'       => '',
        'file.upload'  => true,
        'scope'        => 'email,user_photos,publish_stream,photo_upload,friends_photos,user_photo_video_tags,friends_photo_video_tags',
        'redirect.uri' => 'http://localhost/framework-php/loginfb/',
    ),
);

return $settings;
