<?php return array (
  'root' => 
  array (
    'pretty_version' => 'No version set (parsed as 1.0.0)',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'inkuba/framework_php',
  ),
  'versions' => 
  array (
    'aws/aws-sdk-php' => 
    array (
      'pretty_version' => '3.148.1',
      'version' => '3.148.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '56caf889ddc8dcc5b6999c234998f3b945d787e3',
    ),
    'cocur/slugify' => 
    array (
      'pretty_version' => 'v3.2',
      'version' => '3.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd41701efe58ba2df9cae029c3d21e1518cc6780e',
    ),
    'codeguy/upload' => 
    array (
      'pretty_version' => '1.3.2',
      'version' => '1.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '6a9e5e1fb58d65346d0e557db2d46fb25efd3e37',
    ),
    'doctrine/annotations' => 
    array (
      'pretty_version' => '1.10.3',
      'version' => '1.10.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5db60a4969eba0e0c197a19c077780aadbc43c5d',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
    ),
    'gentle/bitbucket-api' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '178734f8fcbfd569de401d0d65daa3b63a23fa9f',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '7.0.1',
      'version' => '7.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2d9d3c186a6637a43193e66b097c50e4451eaab2',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => 'v1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a59da6cf61d80060647ff4d3eb2c03a2bc694646',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.6.1',
      'version' => '1.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '239400de7a173fe9901b9ac7c06497751f00727a',
    ),
    'icanboogie/inflector' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9fa7e8fcf7a6ac06257ae77e9aa2ec90925b4327',
    ),
    'inkuba/framework_php' => 
    array (
      'pretty_version' => 'No version set (parsed as 1.0.0)',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'ircmaxell/random-lib' => 
    array (
      'pretty_version' => 'v1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0eaad991c1756842f26dfbcbc6effcabb5003d0a',
    ),
    'ircmaxell/security-lib' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '80934de3c482dcafb46b5756e59ebece082b6dc7',
    ),
    'jacobkiers/oauth' => 
    array (
      'pretty_version' => '1.0.12',
      'version' => '1.0.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '95477a77107436e67a10e799fad04d82719a5f1e',
    ),
    'kriswallsmith/buzz' => 
    array (
      'pretty_version' => 'v0.16.1',
      'version' => '0.16.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4977b7d44dbef49cdc641f14be6512fdcfe32f12',
    ),
    'lusitanian/oauth' => 
    array (
      'pretty_version' => 'v0.8.11',
      'version' => '0.8.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc11a53db4b66da555a6a11fce294f574a8374f9',
    ),
    'mtdowling/jmespath.php' => 
    array (
      'pretty_version' => '2.6.0',
      'version' => '2.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '42dae2cbd13154083ca6d70099692fef8ca84bfb',
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
    ),
    'psr/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'slim/slim' => 
    array (
      'pretty_version' => '2.6.3',
      'version' => '2.6.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9224ed81ac1c412881e8d762755e3d76ebf580c0',
    ),
    'slim/strong' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '1af2dc2f49f14521bda280a3b91c8fef54f67fc0',
    ),
    'slim/views' => 
    array (
      'pretty_version' => '0.1.3',
      'version' => '0.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '8561c785e55a39df6cb6f95c3aba3281a60ed5b0',
    ),
    'swiftmailer/swiftmailer' => 
    array (
      'pretty_version' => 'v5.4.12',
      'version' => '5.4.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '181b89f18a90f8925ef805f950d47a7190e9b950',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.1.3',
      'version' => '2.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5e20b83385a77593259c9f8beb2c43cd03b2ac14',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v2.3.42',
      'version' => '2.3.42.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fd6d162d97bf3e6060622e5c015af39ca72e33bc',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v5.1.3',
      'version' => '5.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '4298870062bfc667cb78d2b379be4bf5dec5f187',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1c302646f6efc070cd46856e600e5e0684d6b454',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a6977d63bf9a0ad4c65cd352709e230876f9904a',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v5.1.3',
      'version' => '5.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ea342353a3ef4f453809acc4ebc55382231d4d23',
    ),
    'twig/twig' => 
    array (
      'pretty_version' => 'v1.43.1',
      'version' => '1.43.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2311602f6a208715252febe682fa7c38e56a3373',
    ),
    'wixel/gump' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '3e69c2f5f074fa4fa9052a9f57d0daa23353d8e8',
    ),
    'zircote/swagger-php' => 
    array (
      'pretty_version' => '3.0.4',
      'version' => '3.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa47d62c22c95272625624fbf8109fa46ffac43b',
    ),
  ),
);
