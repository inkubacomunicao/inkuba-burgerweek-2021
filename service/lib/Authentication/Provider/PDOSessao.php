<?php
/**
 * Strong Authentication Library
 *
 * User authentication and authorization library
 *
 * @license     MIT Licence
 * @category    Libraries
 * @author      Andrew Smith
 * @link        http://www.silentworks.co.uk
 * @copyright   Copyright (c) 2012, Andrew Smith.
 * @version     1.0.0
 */

namespace lib\Authentication\Provider;

class PDOSessao extends ProviderSessao
{
    /**
     * @var array
     */
    protected $settings = array(
        'dsn'    => '',
        'dbuser' => null,
        'dbpass' => null,
    );

    protected $fb = null;

    /**
     * Inicializa conexão PDO e faz merge das configurações 
     * do usuário com as configurações default
     *
     * @param array $config
     */
    public function __construct($config, $fb)
    {
        parent::__construct($config);
        $this->config = array_merge($this->settings, $this->config);

        if (!isset($this->config['db']) || !($this->config['db']->getConnection() instanceof \PDO)) {
            throw new \InvalidArgumentException('Deve-se informar um objeto de conexão PDO válido.');
        }

        $this->pdo         = $this->config['db']->getConnection();
        $this->fb          = $fb;
    }

    /**
     * Verificação de login do usuário (sessão)
     *
     * @return boolean
     */
    public function loggedIn()
    {
        return (isset($_SESSION['auth_user']) && !empty($_SESSION['auth_user']));
    }

    /**
     * Busca usuário pelos
     * dados da sessão
     *
     * @return Usuario | null
     */
    public function getUser()
    {
        if (isset($_SESSION['auth_user']['UsuarioID'])) {
            $sql  = "SELECT * FROM tbl_usuarios WHERE UsuarioID = :id and usu_status = 1";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindParam(':id', $_SESSION['auth_user']['UsuarioID']);
            $stmt->execute();

            $user = $stmt->fetch(\PDO::FETCH_OBJ);

            return $user;
        }

        return null;
    } 

    /**
     * Verifica se usuário tem acesso a determinada rota
     *
     * @param string $rota
     *
     * @return boolean
     */
    public function usuarioPertenceGrupo($rota)
    {
        $sql = 'SELECT rot_nome
				FROM tbl_rotas
					INNER JOIN tbl_usuarios_grupo ON FIND_IN_SET(RotaID, gru_rotas)
					INNER JOIN tbl_usuarios u ON FIND_IN_SET(UsuarioGrupoID, usu_grupos)
				WHERE UsuarioID = :id AND 
					  rot_nome = :rota AND 
					  rot_status = 1 AND 
					  usu_status = 1 AND 
					  gru_status = 1';

        $stmt = $this->pdo->prepare($sql);

        $stmt->bindParam(':id', $_SESSION['auth_user']['UsuarioID']);
        $stmt->bindParam(':rota', $rota);
        $stmt->execute();

        $user =  $stmt->fetch();
		
        $res = $user ? true : false;

        return $res;
    }

    /**
     * Autenticar usuário utilizando
     * usuário e senha
     *
     * @param string $usernameOrEmail
     * @param string $password
     *
     * @return boolean
     */
    public function login($usernameOrEmail = null, $password = null)
    {
        if(! is_object($usernameOrEmail)) {
            $sql  = "SELECT *, IF(FIND_IN_SET(1, usu_grupos), 1, 0) AS 'usu_admin' FROM tbl_usuarios WHERE usu_email = :email";
			
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindParam(':email', $usernameOrEmail);
            $stmt->execute();

            $user = $stmt->fetch(\PDO::FETCH_OBJ);
			
        }

        if(is_object($user) && ($user->usu_email === $usernameOrEmail) && crypt($password, $user->usu_senha) === $user->usu_senha) {
			
            return $this->completeLogin($user);
        }

        return false;
    }

    /**
     * Login e armazena detalhes do usuário na sessão
     *
     * @param object $user
     *
     * @return boolean
     */
    protected function completeLogin($user)
    {
        $userInfo = array(
            'UsuarioID' => $user->UsuarioID,
			'usu_admin' => $user->usu_admin,
            'usu_email' => $user->usu_email,
            'logged_in' => true
        );

        return parent::completeLogin($userInfo);
    }

    /**
     * Realiza logout removendo os valores da sessão ou
     * removendo a sessão completamente
     *
     * @param boolean $destroy
     *
     * @return boolean
     */
    //public function logout($destroy = false)
	public function logout()
    {
        session_destroy();
        $_SESSION['auth_user'] = array();

        // Double check
        return !$this->loggedIn();
    }
}
