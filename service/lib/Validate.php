<?php

error_reporting(E_ALL);
ini_set("display_errors",0);

require_once $_SERVER['DOCUMENT_ROOT'] . '/service/lib/ValidaCPFCNPJ.php';

class Validate
{

	public $ValidaCPFCNPJ;
	public function __construct()
	{

		$this->ValidaCPFCNPJ = new ValidaCPFCNPJ;

	}

	public function validaString($text)
	{

		$string = trim($text);
		$string = ucwords(strtolower($string));

		$return = str_replace(
							' Di ',
							' di ',
								str_replace(
									' Do ',
									' do ',
									str_replace(
										' Dos ',
										' dos ',
											str_replace(
											' Da ',
											' da ',
											str_replace(
											' De ',
											' de ',
											ucwords(
											strtolower(
												$string
											)
										)
									)
								)
							)
						)
					);

		$_RETURN['cod'] = 200;
		$_RETURN['string'] = addslashes($return);
		$_RETURN['posted'] = $text;

		return $_RETURN;

	}

	public function validaEmail($email)
	{

		$mail = strtolower(trim($email));
		$mail = trim($mail);

		if(filter_var($mail, FILTER_VALIDATE_EMAIL)){

		    $_RETURN['cod'] = 200;
			$_RETURN['email'] = $mail;

		}else{

		    $_RETURN['cod'] = 500;
		    $_RETURN['email'] = 'E-mail invalido';

		}

		$_RETURN['posted'] = $email;

		return $_RETURN;

	}

	public function validaTelefone($telefone)
	{

		$str = $telefone;
		preg_match_all('!\d+!', $str, $matches);
		$numbers = $matches[0];

		$fullPhone = implode('',$numbers);

		//$_RETURN['fullPhone'] = $fullPhone;

		$_RETURN['cod'] = 500;

		/*
		if(strlen($fullPhone) > 9){

			if(strlen($fullPhone) == 10){

				$_RETURN['ddd']       = substr($fullPhone,0,2);
				$_RETURN['telefone']  = substr($fullPhone,2,8);
				$_RETURN['cod'] = 200;

			}

			if(strlen($fullPhone) == 11){

				$_RETURN['ddd']       = substr($fullPhone,0,2);
				$_RETURN['telefone']  = substr($fullPhone,2,9);
				$_RETURN['cod'] = 200;

			}

			if(strlen($fullPhone) == 12){

				$_RETURN['ddd']       = substr($fullPhone,1,2);
				$_RETURN['telefone']  = substr($fullPhone,3,9);
				$_RETURN['cod'] = 200;

			}

			$telephoneFormated = substr($_RETURN['telefone'],0,5).'-'.substr($_RETURN['telefone'],-4);
			$_RETURN['formated'] = $_RETURN['ddd'].' '.$telephoneFormated;

		}else{

			if(strlen($telefone) == 8){

				$telephoneFormated = substr($telefone,0,4).'-'.substr($telefone,-4);

			}else if(strlen($telefone) == 9){

				$telephoneFormated = substr($telefone,0,5).'-'.substr($telefone,-4);

			}

			$_RETURN['ddd']      = '';
			$_RETURN['telefone'] = $telephoneFormated;
			$_RETURN['cod'] = 200;

		}
		*/

		if( strlen($fullPhone) > 8 ) {

			$_RETURN['ddd']      = '';
			$_RETURN['telefone'] = $fullPhone;
			$_RETURN['formated'] = $_RETURN['telefone'];
			$_RETURN['cod'] = 200;

			$_RETURN['posted'] = $telefone;

		}

		$_RETURN['posted'] = $telefone;

		return $_RETURN;

	}

	public function validaCpf($cpf)
	{

		//$_RETURN = $this->ValidateCPF->validaCPF($cpf);
		$this->ValidaCPFCNPJ->inicia(trim($cpf));
		if($this->ValidaCPFCNPJ->valida()){

			$formatado = $this->ValidaCPFCNPJ->formata();

			$_RETURN['cod'] = 200;
			$_RETURN['type'] = $this->ValidaCPFCNPJ->verifica_cpf_cnpj();
			$_RETURN['formated'] = $formatado;
			$_RETURN['number'] = $this->ValidaCPFCNPJ->naoformatado();

		}else{

			$_RETURN['cod'] = 500;
			$_RETURN['msg'] = 'CPF invalido';

		}

		$_RETURN['posted'] = $cpf;

		return $_RETURN;

	}

	public function validaData($date)
	{

		/* $data = str_replace('/','-',trim($date));
		$ano = substr($data,0,-6);

		if(!is_numeric($ano)){

			$data = implode("-",array_reverse(explode("-",$data)));

		} */

		$year  = substr($date,0,4);
		$month = substr($date,5,2);
		$day   = substr($date,8,2);

		if(checkdate($month, $day, $year)){

			$_RETURN['cod'] = 200;
			$_RETURN['date'] = $date;
			$_RETURN['br_date'] = implode("/",array_reverse(explode("-",$date)));
			$_RETURN['year'] = $year;
			$_RETURN['month'] = $month;
			$_RETURN['day'] = $day;

		}else{

			$_RETURN['cod'] = 500;

		}

		$_RETURN['posted'] = $date;

		return $_RETURN;

	}

	public function validaCep($cep)
	{

		$string = trim($cep);
		$numbers = str_replace("-","",$string);

		if(strlen($numbers) < 8){

			$_RETURN['cod'] = 500;
		    $_RETURN['msg'] = 'CEP inv�lido. Verifique o n�mero de caracteres.';

		}else{

			$_RETURN['cod'] = 200;
			$_RETURN['cep'] = substr($numbers,0,5)."-".substr($numbers,5,3);

		}

		$_RETURN['posted'] = $cep;

		return $_RETURN;

	}

	function requiredFields($obrigatorios,$postado)
	{

		foreach($obrigatorios as $k => $v)
		{

			if(!array_key_exists($v, $postado)){

				$_RETURN['code'] = 500;
				$_RETURN['required_field'] = $v;
				$_RETURN['msg'] = 'Campo "'.$v.'" � obrigat�rio';

				break;
			}

		}

		//cria um array com o posted
		$posted = array();
		array_push($posted,$obrigatorios);
		array_push($posted,$postado);

		$_RETURN['posted'] = $posted;

		return $_RETURN;

	}

}
/*
$Validate = new Validate;
/*
echo "<hr><p>Testa String</p>";
$string = $Validate->validaString('marcelo ant�nio b elias');
print_r($string);echo "<br>";
$string = $Validate->validaString('marcelo a b elias');
print_r($string);echo "<br>";
$string = $Validate->validaString('nome de algu�m');
print_r($string);echo "<br>";
$string = $Validate->validaString('04066-000');
print_r($string);echo "<br>";

echo "<hr><p>Testa CEP</p>";
$cep = $Validate->validaCep('0406600');
print_r($cep);echo "<br>";
$cep = $Validate->validaCep('04066000');
print_r($cep);echo "<br>";
$cep = $Validate->validaCep('04066-000');
print_r($cep);echo "<br>";

echo "<hr>Testa E-mail</p>";
$email = $Validate->validaEmail($Validate->validaString('marcelo.ELIAS@inkuba.com'));
print_r($email);echo "<br>";
$email = $Validate->validaEmail('marcelo.ELIAS@inkuba.com.br');
print_r($email);echo "<br>";
$email = $Validate->validaEmail('marcelo.ELIASinkuba.com.br');
print_r($email);echo "<br>";
$email = $Validate->validaEmail('marcelo.ELIAS@inkuba');
print_r($email);echo "<br>";
$email = $Validate->validaEmail('marcelo');
print_r($email);echo "<br>";


echo "<hr><p>Testa Telefone</p>";
$telefone = $Validate->validaTelefone('84519918');
print_r($telefone);echo "<br>";
$telefone = $Validate->validaTelefone('1184519918');
print_r($telefone);echo "<br>";
$telefone = $Validate->validaTelefone('984519918');
print_r($telefone);echo "<br>";
$telefone = $Validate->validaTelefone('11984519918');
print_r($telefone);echo "<br>";
$telefone = $Validate->validaTelefone('(11) 984519918');
print_r($telefone);echo "<br>";
$telefone = $Validate->validaTelefone('(11)9.8451.9918');
print_r($telefone);echo "<br>";
$telefone = $Validate->validaTelefone('(011) 9.8451.9918');
print_r($telefone);echo "<br>";

echo "<hr><p>Testa CPF</p>";

$cpf = $Validate->validaCpf('10206784000162');
print_r($cpf);echo "<br>";
$cpf = $Validate->validaCpf('315.877.518-29');
print_r($cpf);echo "<br>";
$cpf = $Validate->validaCpf('315.877.518-28');
print_r($cpf);echo "<br>";
$cpf = $Validate->validaCpf('315877.518-28');
print_r($cpf);echo "<br>";
$cpf = $Validate->validaCpf('315.87751828');
print_r($cpf);echo "<br>";
$cpf = $Validate->validaCpf('31587751828');
print_r($cpf);echo "<br>";

echo "<hr><p>Testa Data</p>";

$data = $Validate->validaData('32/05/1983');
print_r($data);echo "<br>";
$data = $Validate->validaData('11/05/1983');
print_r($data);echo "<br>";
$data = $Validate->validaData('1983-05-11');
print_r($data);echo "<br>";
$data = $Validate->validaData('1983-14-11');
print_r($data);echo "<br>";
$data = $Validate->validaData('11-05-1983');
print_r($data);echo "<br>";
$data = $Validate->validaData('1983/05/11');
print_r($data);echo "<br>";
*/
