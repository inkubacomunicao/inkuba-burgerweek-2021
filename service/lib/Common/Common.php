<?php

namespace lib\Common;

/**
 * COMMON - Funções comuns de apoio ao sistema
 */
 
use app\model\Ambientes;

class Common
{

    public function __construct()
    {

    }

	// Senha aleatoria
	function randomPass($length) {
		$alpha = "abcdefghijklmnopqrstuvwxyz";
		$alpha_upper = strtoupper($alpha);
		$numeric = "0123456789";
		 
		// default [a-zA-Z0-9]{9}
		$chars = $alpha . $alpha_upper . $numeric;
		$lengthQ = $length;
		 
		$len = strlen($chars);
		$pw = '';
		 
		for ($i=0;$i<$lengthQ;$i++)
				$pw .= substr($chars, rand(0, $len-1), 1);
		 
		// the finished password
		$pw = str_shuffle($pw);	
		
		return $pw;
	}	


}