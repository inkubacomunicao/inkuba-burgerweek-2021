var express = require('express'),
    fs = require('fs'),
    path = require('path'),
    favicon = require('serve-favicon'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    cors = require('./utils/cors'),
    app = express();

router = express.Router();

// view engine setup
// app.set( 'views', path.join( __dirname, '/' ) );
// app.set( 'view engine', 'html' );

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use( logger( 'dev' ) );
app.use( bodyParser.json() );
app.use( bodyParser.urlencoded( { extended: false } ) );
app.use( cookieParser() );

app.use( express.static( path.resolve( __dirname, 'build' ) ) );

app.use( function( req, res, next ) {

  if( req.headers.host.match(/^www/) !== null || req.headers[ 'x-forwarded-proto' ] == 'http' ) {

    res.redirect( 'https://' + req.headers.host.replace(/^www\./, '') + req.url, 301 );

  } else {

    next();

  }

} );

app.all( '*', cors, function( req, res, next ) {

  res.sendfile( __dirname + '/build/index.html' );

} );

module.exports = app;
