import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Pubsub from 'pubsub-js';
import { spinnerService } from '@chevtek/react-spinners';
import ReactGA from 'react-ga';
import ScrollToTop from 'react-router-scroll-top';
import { disableBodyScroll, enableBodyScroll } from "body-scroll-lock";

// Worker
import registerServiceWorker from './registerServiceWorker';

// CSS vendors
import 'reset-css/sass/_reset.scss';
import '@material/layout-grid/dist/mdc.layout-grid.min.css';

// CSS default
import './resource/styles/scss/helpers/fonts.scss';
import './resource/styles/scss/helpers/animates.scss';
import './resource/styles/scss/default.scss';

// Common

// Pages
import Hotsite from './modules/pages/Hotsite';
import Terms from './modules/pages/Terms';

// Structure and routing
class Page extends Component {

  disableBody = target => disableBodyScroll( target );
  enableBody = target => enableBodyScroll( target );

  pageBehavior( data ) {
    document.body.classList.toggle( 'lock', data.stateMenu );
  }

  componentDidMount() {
    // const options = {
    //   autoConfig: true, 	// set pixel's autoConfig
    //   debug: false, 		// enable logs
    // };

    // Facebook init
    // ReactPixel.init( '129648127951381', {}, options );
    // ReactPixel.pageView();
    // ReactPixel.track( 'ViewContent', {} );

    // GA init
    let uaCode = 'UA-176056713-2';

    ReactGA.initialize( uaCode, { debug: false } );
    let uri = window.location.pathname + window.location.search;
    ReactGA.pageview( uri );

    Pubsub.subscribe( 'flip-menu', ( ref, data ) => {

      try {

        this.pageBehavior( data );

      } catch ( err ) {

        console.error( 'Page subscribe flip-menu' );
        console.error( err );

      }

    } );

    Pubsub.subscribe( 'close-menu', ( ref, data ) => {

      try {

        this.pageBehavior( data );

      } catch ( err ) {

        console.error( 'Page subscribe close-menu' );
        console.error( err );

      }

    } );

    Pubsub.subscribe( 'lock-page', ( ref, data ) => {

      try {

        this.pageBehavior( data );

      } catch ( err ) {

        console.error( 'Page subscribe flip-menu' );
        console.error( err );

      }

    } );

  }

  close() {
    spinnerService.hideAll();
  }

  middleware( Section ) {

    // ReactPixel.pageView();
    // ReactPixel.track( 'ViewContent', {} );

    let uri = window.location.pathname + window.location.search;
    ReactGA.pageview( uri );

    return <Section />;

  }

  render() {
    return (
      <div>
        <Switch>
          <Route exact path='/' render={ () => { return this.middleware( Hotsite ) } } />
          <Route exact path='/termos-de-uso' render={ () => { return this.middleware( Terms ) } } />
        </Switch>
      </div>
    )
  }
}

try {

  ReactDOM.render( (
    <BrowserRouter>
      <ScrollToTop>
        <Page />
      </ScrollToTop>
    </BrowserRouter>
  ), document.getElementById( 'root' ) );

} catch ( err ) {

  console.error( 'Root error' );
  console.error( err );

}

registerServiceWorker();
