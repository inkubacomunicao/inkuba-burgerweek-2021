import React, { Component } from 'react';
import ReactSVG from 'react-svg';

// Common

// Images
import spinner from '../../resource/images/svg/spinner.svg';

// SVGs

// CSS
import '../../resource/styles/scss/partials/loader.scss';

export default class Loader extends Component {

  render() {
    return (
      <section className="box-loader">
        <div className="container">
          <ReactSVG path={ spinner } svgClassName="Loader" />
          <span>carregando mais restaurantes</span>
        </div>
      </section>
    );
  }

}
