import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
// import ReactGA from 'react-ga';

// Common

// SVGs

// Images
import ig from '../../resource/images/icon-ig.png';
import logoInkuba from '../../resource/images/logo-inkuba.png';

// CSS
import '../../resource/styles/scss/partials/footer.scss';

export default class Footer extends Component {
  constructor( props ) {
    super( props );

    this.state = {
      auth: false,
      accountAddress: false
    };
  }

  async componentDidMount() {

    try {


    } catch ( err ) {

      console.error( 'componentDidMount Footer' );
      console.error( err );

    }

  }

  mountAuth( auth, accountAddress ) {

    this.setState( {
      auth: auth,
      accountAddress: accountAddress
    } );

  }

  openLink( e ) {
    let otherWindow = window.open();
    otherWindow.opener = null;
    otherWindow.location = e.currentTarget.href;

    e.preventDefault();
  }

  render() {
    return (
      <footer id="footer">
        <div className="lnk-instagram">
          <a href="https://www.instagram.com/burgerfestoficial/" onClick={ this.openLink.bind( this ) }>
            <img src={ ig } alt="Instagram"/>
            <span>@burgerfestoficial</span>
          </a>
        </div>

        <div className="quick-text">
          <div className="mdc-layout-grid container-text">
            <div className="mdc-layout-grid__inner">
              <div className="mdc-layout-grid__cell--span-12-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-4-phone center">
                <p>Este é um site estritamente promocional, no qual é divulgada a promoção denominada “Semana do Hambúrguer” e onde podem ser encontrados links de acesso aos canais dos restaurantes participantes. Não é realizado qualquer tipo de venda nesta plataforma. Beba com moderação. Não compartilhe com menores de 18 anos.</p>
              </div>

              <div className="mdc-layout-grid__cell--span-12-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-4-phone center">
                <p style={{lineHeight: '16px'}}>
                  © 2022 - Semana do Hambúrguer - Todos os direitos reservados
                  <br/>
                  <NavLink exact to="termos-de-uso" className="" style={ { color: "#525252", textDecoration: "underline" } }>Termos de uso e política de privacidade</NavLink>
                </p>

                <a href="https://inkuba.com.br/" onClick={ this.openLink.bind( this ) }><img src={ logoInkuba } alt="Inkuba"/></a>
              </div>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}
