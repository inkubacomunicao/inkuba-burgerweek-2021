import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import ReactSVG from 'react-svg';
import Pubsub from 'pubsub-js';
import { Link } from "react-scroll";

// Common

// SVGs

// Images
import ig from '../../resource/images/icon-ig.png';
import home from '../../resource/images/svg/icon-home.svg';

// CSS
import '../../resource/styles/scss/partials/header.scss';
import '../../resource/styles/scss/partials/menu.scss';
import '../../resource/styles/scss/partials/desktop-menu.scss';

export default class Header extends Component {
  constructor( props ) {
    super( props );

    this.state = {
      stateMenu: false,
      auth: false,
      type: props.type,
      accountAddress: ''
    };
  }

  async componentDidMount() {

    try {


    } catch ( err ) {

      console.error( 'componentDidMount Header' );
      console.error( err );

    }

  }

  mountAuth( auth, accountAddress ) {

    this.setState( {
      auth: auth,
      accountAddress: accountAddress
    } );

  }

  flipMenu() {
    this.setState( () => ( {
        stateMenu: !this.state.stateMenu
      } )
    );

    Pubsub.publish( 'flip-menu', { stateMenu: !this.state.stateMenu } );
  }

  closeMenu() {
    this.setState( () => ( {
        stateMenu: false
      } )
    );

    Pubsub.publish( 'close-menu', { stateMenu: false } );
  }

  openLink( e ) {
    let otherWindow = window.open();
    otherWindow.opener = null;
    otherWindow.location = e.currentTarget.href;

    this.closeMenu();

    e.preventDefault();
  }

  render() {
    return (
      <header id="header" className="any-moment-fixed">
        <div className="mask-menu" onClick={ this.closeMenu.bind( this ) }></div>

        <div className="mdc-layout-grid box-header">
          <div className="">
            <nav id="menu" className={ this.state.stateMenu ? 'activate' : '' }>

              <div className="burger" onClick={ this.flipMenu.bind( this ) }>
                <span></span>
                <span></span>
                <span></span>
              </div>

              <ul className="list-menu">
                <li className="item-menu">

                  { this.state.type === "terms" ? (
                    <NavLink exact to="/" className="box-home" onClick={ this.closeMenu.bind( this ) }>
                      <h2>Site</h2>

                      <ReactSVG path={ home } svgClassName="icon-home" />
                    </NavLink>
                  ) : null }

                  <ul className="list">
                    { this.state.type === "home" ? (
                      <li className="item">
                        <NavLink exact to="termos-de-uso" onClick={ this.closeMenu.bind( this ) }>
                          Termos de Uso
                        </NavLink>
                      </li>
                    ) : null }

                    { this.state.type === "terms" ? (
                      <li className="item">
                        <NavLink exact to="/" onClick={ this.closeMenu.bind( this ) }>
                          Voltar para a home
                        </NavLink>
                      </li>
                    ) : null }

                    { this.state.type === "home" ? (
                      <li className="item">
                        <Link
                          activeClass="activate"
                          to="promo"
                          spy={true}
                          smooth={true}
                          offset={0}
                          duration={500}
                          onClick={ this.closeMenu.bind( this ) }>
                          O Festival
                        </Link>
                      </li>
                    ) : null }

                    { this.state.type === "home" ? (
                      <li className="item">
                        <Link
                          activeClass="activate"
                          to="stores"
                          spy={true}
                          smooth={true}
                          offset={-10}
                          duration={500}
                          onClick={ this.closeMenu.bind( this ) }>
                          Restaurantes Participantes
                        </Link>
                      </li>
                    ) : null }

                  </ul>
                </li>

                <li className="item-menu">
                  <h2>Redes sociais</h2>
                  <ul className="list">
                    <li className="item">
                      <a href="https://www.instagram.com/burgerfestoficial" onClick={ this.openLink.bind( this ) }>
                        <img src={ ig } alt="Instagram"/>
                        @burgerfestoficial
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </header>
    );
  }
}
