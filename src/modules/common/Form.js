import React, { Component } from 'react';

import CreatableSelect from 'react-select/creatable';
// import ReactSVG from 'react-svg';
// import _ from 'underscore';

// Common

// SVGs

// CSS
// import '../../resource/styles/scss/partials/form.scss';

const customStyles = {
    dropdownIndicator: (provided, state) => ({
      ...provided,
      display: 'none'
    }),
    indicatorSeparator: (provided, state) => ({
      ...provided,
      display: 'none'
    }),
    control: (provided, state) => ({
      ...provided,
      borderRadius: '55px',
      height: '50px',
      borderWidth: '0',
      borderColor: 'transparent',
      boxShadow: state.isFocused ? '0 0 2px 0px #111111' : 'none',
      backgroundColor: '#ce1a30',
      outline: 'none',
    }),
    placeholder: (provided, state) => ({
      ...provided,
      width: '100%',
      padding: '0',
      margin: '0',
      left: '0',
      color: 'white',
      textAlign: 'center',
      fontSize: '21px'
    }),
    valueContainer: (provided, state) => ({
      ...provided,
      overflow: 'visible'
    }),
    singleValue: (provided, state) => ({
      ...provided,
      color: 'white',
      fontSize: '18px',
      overflow: 'visible'
    }),
    clearIndicator: (provided, state) => ({
      ...provided,
      fill: 'white',
      stroke: 'white',
      color: 'white',
    }),
    indicatorsContainer: (provided, state) => ({
      ...provided,
      cursor: 'ponter'
    }),
}

export class CreatableSingle extends Component {
  constructor( props ) {
    super( props );

    this.state = {
      id: props.id,
      placeholder: props.placeholder,
      options: props.options,
      onChange: props.onChange
    };
  }

  componentWillReceiveProps( nextProps ) {
    this.setState( {
      options: nextProps.options
    } );
  }

  setValue( opt ) {
    let value = '';
    if( opt && opt.value.length ) {
      value = opt.value;
    }

    this.state.onChange( value );
  }

  render() {
    return (
      <CreatableSelect
        styles={ customStyles }
        formatCreateLabel={ ( inputValue: string ) => `Filtrar por: ${inputValue}` }
        placeholder={ this.state.placeholder }
        isClearable
        onChange={ this.setValue.bind( this ) }
        onInputChange={ this.handleInputChange }
        options={ this.state.options }
      />
    );
  }
}
