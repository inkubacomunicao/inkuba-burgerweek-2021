import React, { Component } from 'react';
import axios from 'axios';
import ReactSVG from 'react-svg';

// Common

// SVGs
import iconPause from '../../resource/images/svg/icon-pause.svg';

// Images

// CSS
import '../../resource/styles/scss/partials/gallery.scss';

export default class Gallery extends Component {

  constructor( props ) {
    super( props );

    this.state = {
      envExt: 'https://semanadohamburguer.com.br/service/destaques',
      date: new Date(),
      paused: false,
      count: 0,
      select: 0,
      time: 6,
      scenes: []
    };
  }

  async componentDidMount() {

    try {

      await this.scenes();

    } catch ( err ) {

      console.error( 'componentDidMount QuickStartBar' );
      console.error( err );

    }

  }

  componentWillUnmount() {

    clearInterval( this.timerBar );
    clearInterval( this.timerDelay );

  }

  componentWillReceiveProps( nextProps ) {
    this.setState( {
      scenes: nextProps.scenes
    } );
  }

  async scenes() {

    try {

      let result = await axios.get( this.state.envExt );

      if( result && result.data.cod === 200 ) {

        this.setState( { scenes: result.data.res } );

        this.startScene();

      }

    } catch ( err ) {

      console.error( 'BetaForm filter' );
      console.error( err );

    }

  }

  startScene() {

    if( this.state.scenes.length > 1 ) {

      this.timerBar = setInterval( () => {

        if( !this.state.paused ) {

          this.tick();

        }

      }, 1000 );

    }

  }

  tick() {

    let count = this.state.count,
        change = count >= this.state.time,
        select = this.state.select;

    if( change ) {

      select = select + 1;

      if( this.state.scenes && select >= this.state.scenes.length ) {

        select = 0;

      }

    }

    this.setState( {
      date: new Date(),
      select: select,
      count: ( change ? 0 : count + 1 )
    } );

  }

  openLink( e ) {
    let otherWindow = window.open();
    otherWindow.opener = null;
    otherWindow.location = e.currentTarget.href;

    e.preventDefault();
  }

  pressStart( e ) {

    this.timerDelay = setTimeout( () => {

      this.setState( {
        paused: true
      } );

    }, 200 );

  }

  pressOver( e ) {

    clearInterval( this.timerDelay );

    this.setState( {
      paused: false
    } );

  }

  changeScene( id ) {

    clearInterval( this.timerBar );
    clearInterval( this.timerDelay );

    this.setState( {
      date: new Date(),
      select: id,
      count: id
    } );

    this.startScene();

  }

  render() {
    return (
      <div id="gallery" className={ this.state.paused ? "any-moment-fixed paused" : "any-moment-fixed" } onMouseLeave={ this.pressOver.bind( this ) } onMouseDown={ this.pressStart.bind( this ) } onMouseUp={ this.pressOver.bind( this ) } onTouchStart={ this.pressStart.bind( this ) } onTouchEnd={ this.pressOver.bind( this ) } style={ this.state.scenes.length == 0 ? {display: 'none'} : null }>
        { this.state.scenes && this.state.scenes.map( ( scene, i ) =>
          <div key={ i } className={ this.state.select === i ? "item activated" : "item" }>

              { scene.deh_link === "" ? (

                <div className="box-content">
                  <div className="img hide-phone hide-tablet" style={ { backgroundImage: `url(${scene.deh_imagem})` } }></div>
                  <div className="img hide-desktop" style={ { backgroundImage: `url(${scene.deh_imagem_mobile})` } }></div>
                </div>

              ) : (

                <div className="box-content">
                  <a href={ scene.deh_link } target="_blank" onClick={ this.openLink.bind( this ) } className="img hide-phone hide-tablet" style={ { display: 'block', backgroundImage: `url(${scene.deh_imagem})` } }></a>
                  <a href={ scene.deh_link } target="_blank" onClick={ this.openLink.bind( this ) } className="img hide-desktop" style={ { display: 'block', backgroundImage: `url(${scene.deh_imagem_mobile})` } }></a>
                </div>

              ) }

            { this.state.scenes.length > 1 ? (

              <div className="live-line" style={{ animationDuration: `${ this.state.time + 0.5 }s` }}></div>

            ) : null }
          </div>
        ) }

        <ReactSVG path={ iconPause } svgClassName="icon-pause" />

        <div className="bullets">
          { this.state.scenes && this.state.scenes.map( ( scene, i ) =>
            <div key={ i } className={ this.state.select === i ? "bullet activated" : "bullet" } onClick={ this.changeScene.bind( this, i ) }></div>
          ) }
        </div>
      </div>
    );
  }
}
