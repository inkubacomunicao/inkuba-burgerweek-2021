
export default ( {
  limitForNew: 7,
  searchIdentifierForNew: 'há',
  rules: {
    "jsx-a11y/anchor-is-valid": [ "error", {
      "components": [ "Link" ],
      "specialLink": [ "to" ]
    }]
  }
} )
