import React, { Component } from 'react';

import Modal from "react-animated-modal";
import Pubsub from 'pubsub-js';

import ReactSVG from 'react-svg';

// Common

// SVGs
import iconLike from '../../resource/images/svg/icon-heart.svg';

// Images
import thumb from '../../resource/images/thumb.jpg';
import ig from '../../resource/images/icon-ig-red.png';

// CSS
import '../../resource/styles/scss/partials/modal.scss';
import '../../resource/styles/scss/partials/like.scss';

export default class Card extends Component {

  constructor( props ) {
    super( props );

    this.state = {
      showModal: false,
      id: false,
      title: false,
      address: false,
      city: false,
      state: false,
      lat: false,
      lon: false,
      schedule: false,
      photo: false,
      phone: false,
      cardapio: [],
      instagram: false,
      facebook: false,
      twitter: false,

      reserva: false,
      whatsapp: false,
      ifood: false,
      rappi: false,
      delivery: false,
      ubereats: false,
      goomer: false,
      app: false,
      others: false,
      site: false,
      takeout: false,
      liked: false,
    };

  }

  async componentDidMount() {

    try {

      Pubsub.subscribe( 'modal-open', ( ref, data ) => {

        try {

          this.openModal( data.store );

        } catch ( err ) {

          console.error( 'Page subscribe open-modal' );
          console.error( err );

        }

      } );

    } catch ( err ) {

      console.error( 'componentDidMount' );
      console.error( err );

    }

  }

  openLink( e ) {
    let otherWindow = window.open();
    otherWindow.opener = null;
    otherWindow.location = e.currentTarget.href;

    e.preventDefault();
  }

  async openModal( store ) {

    this.setState( {
      showModal: true,
      id: store.RestauranteID,
      title: store.res_multiplos == 's' ? `${store.res_nome} - ${store.res_bairro}` : store.res_nome || '',
      address: store.res_endereco || '',
      city: store.res_cidade || '',
      state: store.res_estado || '',
      lat: store.res_latitude || '',
      lon: store.res_longitude || '',
      schedule: store.res_funcionamento || '',
      photo: store.res_imagem || '',
      phone: store.res_telefone || '',
      cardapio: store.res_cardapio || [],
      instagram: store.res_instagram || '',
      facebook: store.res_facebook || '',
      twitter: store.res_twitter || '',

      reserva: store.res_reserva || false,
      whatsapp: store.res_whatsapp || false,
      ifood: store.res_ifood || false,
      rappi: store.res_rappi || false,
      delivery: store.res_delivery_direto || false,
      ubereats: store.res_ubereats || false,
      goomer: store.res_goomer || false,
      app: store.res_aplicativo_proprio || false,
      takeout: store.res_takeout || false,
      others: store.res_outros || false,
      site: store.res_site || false,
      liked: store.liked || false,
    }, () => {

      Pubsub.publish( 'lock-page', { stateMenu: true } );

    } );

  }

  render() {

    return (
        <Modal
            visible={ this.state.showModal }
            closemodal={ () => this.setState( { showModal: false }, () => { Pubsub.publish( 'lock-page', { stateMenu: false } ) } ) }
            type="fadeInDown">

          <div className="modal">
            <div className="wrap-thumb">
              { this.state.photo ? (
                  <div style={ { backgroundImage: `url(${this.state.photo})` } } className="thumb" alt={ this.state.res_nome }></div>
              ) : (
                  <div style={ { backgroundImage: `url(${thumb})` } } className="thumb" alt={ this.state.res_nome }></div>
              ) }
            </div>

            <div className={ this.state.liked ? 'wrap-like plus activated' : 'wrap-like plus' } onClick={ () => this.setState( { liked: !this.state.liked }, () => { Pubsub.publish( `liked-${this.state.id}`, { id: this.state.id } ) } ) }>
              <ReactSVG path={ iconLike } svgClassName="" />
            </div>

            <div className="wrap-info">
              <div className="mdc-layout-grid container">
                <div className="mdc-layout-grid__inner">
                  <div className="mdc-layout-grid__cell--span-12-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-4-phone">
                    <h3 dangerouslySetInnerHTML={{__html: this.state.title}} />
                  </div>

                  <div className="mdc-layout-grid__cell--span-6-desktop mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-4-phone">
                    {/*<a href={ `https://www.google.com/maps?saddr=${ position.coords.latitude },${ position.coords.longitude }&daddr=${ this.state.lat },${ this.state.lon }` } dangerouslySetInnerHTML={{__html: this.state.address}} onClick={ this.openLink.bind( this ) } />*/}
                    <a className="lnk-address" href={ `https://www.google.com/maps?daddr=${ this.state.lat },${ this.state.lon }` } onClick={ this.openLink.bind( this ) }>
                      { `${ this.state.address }, ${ this.state.city } - ${ this.state.state } ` }
                    </a>
                  </div>
                  <div className="mdc-layout-grid__cell--span-6-desktop mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-4-phone">
                    <div className="lnk-instagram">
                      { this.state.instagram
                          ? <a href={ this.state.instagram } onClick={ this.openLink.bind( this ) }>
                            <img src={ ig } alt="Instagram"/>
                            <span>@{this.state.instagram.replace("https://www.instagram.com/", "")}</span>
                          </a>
                          : <a href="https://www.instagram.com/semanadohamburguer" onClick={ this.openLink.bind( this ) }>
                            <img src={ ig } alt="Instagram"/>
                            <span>@semanadohamburguer</span>
                          </a>
                      }

                    </div>
                  </div>

                  {/* <div className="mdc-layout-grid__cell--span-6-desktop mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-4-phone">
                    { this.state.cardapio.length > 0 ? (
                        <ul className="list">
                          { this.state.cardapio.map( ( item, i ) => (
                              <li key={ item.CardapioID } className="item">
                                <div className="name">
                                  <span>{ item.car_nome }</span>
                                  <span className="price">R${ item.car_valor }{ String(item.car_valor) === "10" && <span>*</span>}</span>
                                </div>
                                <div className="text">
                                  { item.car_descricao }
                                  { String(item.car_valor) === "10" &&
                                  <p style={{fontStyle: "italic"}}>*A promoção de hambúrgueres a R$ 10 é válida somente no dia 28/5</p>
                                  }
                                </div>
                              </li>
                          ) ) }
                        </ul>
                    ) : null }
                  </div> */}

                  <div className="mdc-layout-grid__cell--span-6-desktop mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-4-phone">
                    { this.state.app ? (
                        <a href={ this.state.app } className="btn-store" onClick={ this.openLink.bind( this ) }>
                          <span><small className="bold">Pedir</small> via <small className="bold">App</small></span>
                        </a>
                    ) : null }

                    { this.state.site ? (
                        <a href={ this.state.site } className="btn-store" onClick={ this.openLink.bind( this ) }>
                          <span><small className="bold">Pedir</small> via <small className="bold">Site</small></span>
                        </a>
                    ) : null }

                    { this.state.reserva ? (
                        <a href={ this.state.reserva } className="btn-store" onClick={ this.openLink.bind( this ) }>
                          <span><small className="bold">Pedir</small> via <small className="bold">Reserva</small></span>
                        </a>
                    ) : null }

                    { this.state.delivery ? (
                        <a href={ this.state.delivery } className="btn-store" onClick={ this.openLink.bind( this ) }>
                          <span><small className="bold">Pedir</small> via <small className="bold">Delivery Próprio</small></span>
                        </a>
                    ) : null }

                    { this.state.whatsapp ? (
                        <a href={ this.state.whatsapp } className="btn-store" onClick={ this.openLink.bind( this ) }>
                          <span><small className="bold">Pedir</small> via <small className="bold">Whatsapp</small></span>
                        </a>
                    ) : null }

                    { this.state.ifood ? (
                        <a href={ this.state.ifood } className="btn-store" onClick={ this.openLink.bind( this ) }>
                          <span><small className="bold">Pedir</small> via <small className="bold">Ifood</small></span>
                        </a>
                    ) : null }

                    { this.state.rappi ? (
                        <a href={ this.state.rappi } className="btn-store" onClick={ this.openLink.bind( this ) }>
                          <span><small className="bold">Pedir</small> via <small className="bold">Rappi</small></span>
                        </a>
                    ) : null }

                    { this.state.ubereats ? (
                        <a href={ this.state.ubereats } className="btn-store" onClick={ this.openLink.bind( this ) }>
                          <span><small className="bold">Pedir</small> via <small className="bold">Uber Eats</small></span>
                        </a>
                    ) : null }

                    { this.state.goomer ? (
                        <a href={ this.state.goomer } className="btn-store" onClick={ this.openLink.bind( this ) }>
                          <span><small className="bold">Pedir</small> via <small className="bold">Goomer</small></span>
                        </a>
                    ) : null }

                    { this.state.takeout ? (
                        <a href={ this.state.takeout } className="btn-store" onClick={ this.openLink.bind( this ) }>
                          <span><small className="bold">Pedir</small> via <small className="bold">Takeout</small></span>
                        </a>
                    ) : null }

                    { this.state.others ? (
                        <a href={ this.state.others } className="btn-store" onClick={ this.openLink.bind( this ) }>
                          <span><small className="bold">Pedir</small> via <small className="bold">Outros</small></span>
                        </a>
                    ) : null }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal>
    );
  }
}
