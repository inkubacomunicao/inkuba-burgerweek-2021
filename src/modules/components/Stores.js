import React, { Component } from 'react';
import wurl from 'wurl';

import ReactSVG from 'react-svg';
import axios from 'axios';

// Common
import Loader from '../common/Loader';
import { CreatableSingle } from '../common/Form';

// Components
import Card from '../components/Card';

// SVGs
import iconLocation from '../../resource/images/svg/icon-location.svg';

// Images

// CSS
import '../../resource/styles/scss/partials/stores.scss';
import '../../resource/styles/scss/partials/filter.scss';
import '../../resource/styles/scss/partials/form.scss';

class TextComposer extends Component {
  constructor( props ) {
    super( props );

    this.state = {
      text: props.text || '',
      strong: props.strong || ''
    };
  }

  render() {
    return (
      <span>{ this.state.text } <strong>{ this.state.strong }</strong></span>
    );
  }
}

const envExt = 'https://semanadohamburguer.com.br/service';

export default class Stores extends Component {

  constructor( props ) {
    super( props );

    this.state = {
      filterService: `${ envExt }/filtros`,
      filterStateOpt: [],
      filterCityOpt: [],
      estado: '',
      cidade: '',
      location: false,
      lat: wurl( '?lat' ) || false,
      lon: wurl( '?lon' ) || false,
      timer: false,
      perView: 32,
      currentPage: 0,
      more: true,
      stores: [],
      query: '',
      loading: false,
      isEnd: false

      //RestauranteID
      //res_nome
    };

    this.estadoChange = this.estadoChange.bind( this );
    this.cidadeChange = this.cidadeChange.bind( this );
    this.handleScroll = this.handleScroll.bind(this);

  }

  async componentDidMount() {

    window.addEventListener("scroll", this.handleScroll);

    try {

      let _this = this;

      navigator.geolocation.getCurrentPosition( async function( position ) {

        // console.log( 'position' );
        // console.log( position );

        _this.setState( {
          location: true,
          lat: String( position.coords.latitude ),
          lon: String( position.coords.longitude )
        }, async () => {

          await _this.getDataWithFilter();

        } );

      } );

      await _this.fetchMoreData();

      // Pubsub.publish( 'modal-open', { store: this.state.stores[10] } );

    } catch ( err ) {

      console.error( 'componentDidMount' );
      console.error( err );

    }

  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  async checkLocation() {

    try {

      this.setState( {
        location: !this.state.location
      }, async () => {

        this.getDataWithFilter();

      } );

    } catch ( err ) {

      console.error( 'checkLocation' );
      console.error( err );

    }

  }

  async fetchMoreData() {

    this.setState({isEnd:true});
    await this.getStores();

  };

  async getStores() {

    try {

      let currentPage = this.state.currentPage + 1;

      this.setState( {
        loading: true
      } );

      const stores = await axios.get( `${ envExt }/restaurantes/${currentPage}/${this.state.perView}?${this.state.query}` );

      let url = this.state.filterService;

      if (this.state.estado.length > 0) {
        url = `${ this.state.filterService }/${ this.state.estado }`;
      }

      const filter = await axios.get( url );

      if( filter && filter.data && filter.status === 200 ) {

        let filterStateOpt = [],
            filterCityOpt = [];


        for( let id in filter.data.ufs ) {

          filterStateOpt.push( { value: filter.data.ufs[ id ].uf , label: filter.data.ufs[ id ].uf } );

        }

        for( let id in filter.data.cidades ) {

          filterCityOpt.push( { value: filter.data.cidades[ id ].cidade , label: filter.data.cidades[ id ].cidade } );

        }

        this.setState( {
          filterStateOpt: filterStateOpt,
          filterCityOpt: filterCityOpt
        } );

      }

      if( stores && stores.data.cod === 200 ) {

        this.setState( {
          stores: stores.data.pagina == 1 ? stores.data.res : this.state.stores.concat( stores.data.res ),
          currentPage,
          more: !( stores.data.qtd < this.state.perView || ( this.state.perView * this.state.currentPage ) >= stores.data.total ),
        } );

      } else {

        this.setState( {
          currentPage,
          more: false
        } );

      }

      this.setState( {
        loading: false
      } );

    } catch ( err ) {

      console.error( 'getStores' );
      console.error( err );

    }

  }

  async filter() {

    try {

      let queryString = '';

      if( this.state.cidade.length ) {
        queryString = `${queryString}&cidade=${this.state.cidade}`;
      }

      if( this.state.estado.length ) {
        queryString = `${queryString}&uf=${this.state.estado}`;
      }

      if( this.state.location && this.state.lat.length && this.state.lon.length ) {
        queryString = `${queryString}&lat=${this.state.lat}&lon=${this.state.lon}`;
      }

      this.setState( { query: queryString } );

    } catch ( err ) {

      console.error( 'filter' );
      console.error( err );

    }

  }

  estadoChange( value ) {

    this.setState( { estado: value }, () => {

      clearInterval( this.timer );

      this.timer = setTimeout( async () => {

        // console.log( this.state.estado );

        this.getDataWithFilter();

      }, 1000 );

    } );
  }

  cidadeChange( value ) {

    this.setState( { cidade: value }, () => {

      clearInterval( this.timer );

      this.timer = setTimeout( async () => {

        // console.log( this.state.cidade );

        await this.getDataWithFilter();

      }, 1000 );

    } );

  }

  async getDataWithFilter() {

    await this.resetStores();

  }

  async resetStores() {
    this.setState( {
      currentPage: 0,
      more: true,
      stores: []
    }, async () => {

      await this.filter();

      await this.getStores();

    } );
  }

  openLink( e ) {
    let otherWindow = window.open();
    otherWindow.opener = null;
    otherWindow.location = e.currentTarget.href;

    e.preventDefault();
  }

  handleScroll() {
    const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
    const body = document.body;
    const html = document.documentElement;
    const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
    const windowBottom = windowHeight + window.pageYOffset;
    if (windowBottom >= docHeight-30 && !this.isEnd) {
      this.fetchMoreData();
    } else {
      this.setState({isEnd:false});
    }
  }

  // effects: bounceInDown, pulse, rubberBand, jello, bounceIn, fadeIn, fadeInDown, slideInDown, zoomInUp

  render() {
    return (
      // <section id="stores" style={ this.state.showModal ? { zIndex: '9999999' } : { zIndex: '999' } }>
      // <section id="stores" style={ {borderTop: '80px solid #fff'} }>
      <section id="stores">

        {/* <div id="filter" style={ {display: 'none'} }> */}
        <div id="filter">
          <div className="container">

            <form action="" id="form-filter" method="post">
              <fieldset className="box-filter">

                <div className="mdc-layout-grid container">

                  <div className="mdc-layout-grid__inner">
                    <div className="mdc-layout-grid__cell--span-4-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-4-phone">
                      <h3>Onde você está?</h3>
                    </div>

                    <div className="mdc-layout-grid__cell--span-3-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-3-phone">
                      <CreatableSingle placeholder={ <TextComposer text="selecione seu" strong="Estado" /> } options={ this.state.filterStateOpt } onChange={ this.estadoChange } />
                    </div>

                    <div className="mdc-layout-grid__cell--span-3-desktop mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-3-phone">
                      <CreatableSingle placeholder={ <TextComposer text="selecione sua" strong="Cidade" /> } options={ this.state.filterCityOpt } onChange={ this.cidadeChange } />
                    </div>

                    <div className="mdc-layout-grid__cell--span-1-desktop mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-1-phone">
                      <div className={ ( this.state.location ) ? 'btn-location activated' : 'btn-location' } onClick={ this.checkLocation.bind( this ) }>
                        <ReactSVG path={ iconLocation } svgClassName="icon"/>
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
            </form>
          </div>
        </div>

        <div className="mdc-layout-grid container">

          <div className="mdc-layout-grid__inner">

            <div className="mdc-layout-grid__cell--span-12-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-4-phone" style={{margin: '40px 0'}}>

              <h2>Restaurantes Participantes</h2>

              {/* <h2>Em breve</h2>

              <h3
                style={
                  {
                    textAlign: 'center',
                    color: '#fff',
                    fontSize: '18px',
                    textTransform: 'uppercase'
                  }
                }
              >Restaurantes Participantes</h3> */}

            </div>

          </div>

        </div>

        {/* <div style={ {display: 'none'} }> */}
        <div>

        { this.state.stores.length ? (

          <div className="mdc-layout-grid container">

            <div className="mdc-layout-grid__inner">

            { this.state.stores.map( ( store, i ) => (
              <div key={ store.RestauranteID } className="mdc-layout-grid__cell--span-3-desktop mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-4-phone">

                <Card store={ store }/>

              </div>
            ) ) }

            </div>

            <div className="mdc-layout-grid__inner">

              <div className="mdc-layout-grid__cell--span-12-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-4-phone">

                <div className="loader">

                  { this.state.loading ? (

                    <Loader />

                  ) : this.state.more ? (

                    <div className="btn" onClick={ this.fetchMoreData.bind( this ) } onScroll={this.handleScroll}>
                      Carregar mais
                    </div>

                  ) : null }

                </div>

              </div>

            </div>

          </div>

        ) : !this.state.more ? (

          <div className="mdc-layout-grid container">

            <div className="mdc-layout-grid__inner">

              <div className="mdc-layout-grid__cell--span-12-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-4-phone">
                <div className="warn-stores">Infelizmente não encontramos o que busca.</div>
              </div>

            </div>

          </div>

        ) : null }

        </div>

      </section>
    );
  }
}
