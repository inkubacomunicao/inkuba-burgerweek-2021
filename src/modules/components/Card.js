import React, { Component } from 'react';
import cookie from 'react-cookies';

import Pubsub from 'pubsub-js';

import ReactSVG from 'react-svg';

// Common

// SVGs
import iconLike from '../../resource/images/svg/icon-heart.svg';

// Images
import thumb from '../../resource/images/thumb.jpg';

// CSS
import '../../resource/styles/scss/partials/card.scss';
import '../../resource/styles/scss/partials/like.scss';

export default class Card extends Component {

  constructor( props ) {
    super( props );

    this.state = {
      store: props.store,
      liked: cookie.load( `store-liked-${ props.store.RestauranteID }` ) === 'true' ? true : false
    };

  }

  async componentDidMount() {

    try {

      Pubsub.subscribe( `liked-${this.state.store.RestauranteID}`, ( ref, data ) => {

        try {

          this.toggleLike();

        } catch ( err ) {

          console.error( 'Page subscribe liked' );
          console.error( err );

        }

      } );

    } catch ( err ) {

      console.error( 'componentDidMount' );
      console.error( err );

    }

  }

  toggleLike() {

    let liked = !this.state.liked;

    this.setState( { liked } );

    cookie.save( `store-liked-${ this.state.store.RestauranteID }`, liked );

  }

  openLink( e ) {
    let otherWindow = window.open();
    otherWindow.opener = null;
    otherWindow.location = e.currentTarget.href;

    e.preventDefault();
  }

  async openModal() {

    let store = this.state.store;

    store.liked = this.state.liked;

    Pubsub.publish( 'modal-open', { store } );

  }

  render() {
    return (
      <div className="card">
        <div className="wrap-thumb" onClick={ () => this.openModal() }>
          { this.state.store.res_imagem ? (
            <div style={ { backgroundImage: `url(${this.state.store.res_imagem})` } } className="thumb" alt={ this.state.store.res_nome }></div>
          ) : (
            <div style={ { backgroundImage: `url(${thumb})` } } className="thumb" alt={ this.state.store.res_nome }></div>
          ) }
        </div>

        <div className={ this.state.liked ? 'wrap-like activated' : 'wrap-like' } onClick={ () => this.toggleLike() }>
          <ReactSVG path={ iconLike } svgClassName="" />
        </div>

        {/* <h3 className="store-name" dangerouslySetInnerHTML={{__html: `${this.state.store.res_nome} - ${this.state.store.res_bairro}` }} onClick={ () => this.openModal() } /> */}

        <h3 className="store-name" dangerouslySetInnerHTML={{__html: this.state.store.res_multiplos == 's' ? `${this.state.store.res_nome} - ${this.state.store.res_bairro}` : this.state.store.res_nome }} onClick={ () => this.openModal() } />

        <div className="btn-info" onClick={ () => this.openModal() }>Ver</div>
      </div>
    );
  }
}
