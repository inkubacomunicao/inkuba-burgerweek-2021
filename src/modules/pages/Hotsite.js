import React, { Component } from 'react';

// Common
import Header from '../common/Header';
import Footer from '../common/Footer';
import Gallery from '../common/Gallery';
import Stores from '../components/Stores';
import Modal from '../components/Modal';

// Images
import logo from '../../resource/images/logo-desktop.png';

// Imagens parceiros
import logoGetnet from '../../resource/images/parceiros/parceiros-1.png';
// import logoGoose from '../../resource/images/parceiros/parceiros-2.png';
import logoEsfera from '../../resource/images/parceiros/parceiros-3.png';
// import logoMondial from '../../resource/images/parceiros/parceiros-4.png';

import iconBoy from '../../resource/images/icon-boy.png';
import iconGirl from '../../resource/images/icon-girl.png';
import iconBoyGirl from '../../resource/images/icon-boy-girl.png';
import iconCall from '../../resource/images/icon-call.png';
import iconCallMobile from '../../resource/images/icon-call-mobile.png';

// CSS
import '../../resource/styles/scss/partials/destaque.scss';
import '../../resource/styles/scss/partials/sponsors.scss';
import '../../resource/styles/scss/partials/promo.scss';

export default class Hotsite extends Component {
  // constructor( props ) {
  //   super( props );
  //
  //   // spinnerService.show( 'load' );
  // }

  async componentDidMount() {

    try {

    } catch ( err ) {

      console.error( 'componentDidMount' );
      console.error( err );

    }

    window.addEventListener("scroll", this.onScroll, false);

  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.onScroll, false);
  }

  onScroll = () => {
    if (this.hasReachedBottom()) {
      console.log("FIM");
    }
  };

  hasReachedBottom() {
    return (
        document.body.offsetHeight + document.body.scrollTop ===
        document.body.scrollHeight
    );
  }

  openLink( e ) {
    let otherWindow = window.open();
    otherWindow.opener = null;
    otherWindow.location = e.currentTarget.href;

    e.preventDefault();
  }

  render() {
    return (
      <main className='main'>
        <Header type="home" />
        <Modal />

        <section id="content-page">
          <div id="destaque">
            <img src={ logo } alt="Semana do Hambúrguer"/>
          </div>

          <div id="sponsors">
            <img src={ iconBoyGirl } className="icon-boy-girl" alt=""/>

            <div className="content box">
              <img src={ iconBoy } className="icon-boy" alt=""/>

              <div className="wrap-sponsors">
                <h4>Apoio:</h4>

                <ul className="list">
                  {/* <li className="item">
                    <img src={ logoGoose } className="goose" alt=""/>
                  </li> */}
                  <li className="item">
                    <img src={ logoGetnet } className="getnet" alt=""/>
                  </li>
                  <li className="item">
                    <img src={ logoEsfera } className="esfera" alt=""/>
                  </li>
                  {/* <li className="item">
                    <img src={ logoMondial } className="mondial" alt=""/>
                  </li> */}
                </ul>
              </div>

              <img src={ iconGirl } className="icon-girl" alt=""/>
            </div>
          </div>

          <Gallery />

          <section id="promo">
            <div className="container mdc-layout-grid" style={ {paddingBottom: '0'} }>
              <div className="mdc-layout-grid__inner">
                <div className="col-1 mdc-layout-grid__cell--span-6-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-4-phone" style={ {paddingBottom: '0'} }>
                  <img src={ iconCall } className="icon-call hide-phone" alt=""/>
                  <img src={ iconCallMobile } className="icon-call hide-tablet hide-desktop" alt=""/>
                </div>

                <div className="col-2 mdc-layout-grid__cell--span-6-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-4-phone">
                  <div className="description">
                    <p>Para celebrar o <strong>Dia Mundial do Hambúrguer</strong> criamos esta plataforma onde você pode prestigiar suas hamburguerias preferidas, conhecer novas casas e curtir burgers e combos em promoções incríveis!</p>

                    <p><strong>Até dia 5 de junho</strong>, em várias cidades do país, craques do burger oferecem sugestões e preços especiais.</p>

                    <p><strong>Participe! Peça o seu!</strong></p>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <Stores />

        </section>

        <Footer />
      </main>
    );
  }
}
