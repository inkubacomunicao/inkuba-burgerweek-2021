import React, { Component } from 'react';

// Common
import Header from '../common/Header';
import Footer from '../common/Footer';

// Images
import logo from '../../resource/images/logo-desktop.png';

// CSS
import '../../resource/styles/scss/partials/destaque.scss';
import '../../resource/styles/scss/partials/terms.scss';

export default class Terms extends Component {
  // constructor( props ) {
  //   super( props );
  //
  //   // spinnerService.show( 'load' );
  // }

  async componentDidMount() {

    try {

    } catch ( err ) {

      console.error( 'componentDidMount' );
      console.error( err );

    }

  }

  openLink( e ) {
    let otherWindow = window.open();
    otherWindow.opener = null;
    otherWindow.location = e.currentTarget.href;

    e.preventDefault();
  }

  render() {
    return (
      <main className='main'>
        <Header type="terms" />

        <section id="content-page">
          <div id="destaque" className="fix">
            <img src={ logo } alt="Santander e Heinz apresentam Semana do Hambúrguer"/>
          </div>

          <div id="terms">
            <div className="container mdc-layout-grid">
              <div className="mdc-layout-grid__inner">
                <div className="col-1 mdc-layout-grid__cell--span-12-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-4-phone">
                  <h2>TERMOS DE USO E POLÍTICA DE PRIVACIDADE</h2>
                </div>
                <div className="col-1 mdc-layout-grid__cell--span-12-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-4-phone">
                  <p>Em vigor em 01 de Maio de 2022</p>
                </div>
                <div className="col-1 mdc-layout-grid__cell--span-12-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-4-phone">
                  <p>Estes são os Termos de Uso e a Política de Privacidade que regem o uso do nosso site e fornecem informações sobre a promoção, descritos abaixo. A utilização do nosso site e a participação na promoção pressupõem a concordância com este documento. Portanto, é importante que todos os usuários o leiam com atenção.</p>
                  <p>Os presentes Termos poderão, a qualquer tempo, ter seu conteúdo modificado para adequações e inserções, visando seu aprimoramento. As novas condições entrarão em vigência assim que veiculadas no site.</p>
                  <br/><br/>
                  <p><strong>1. SOBRE A SEMANA DO BURGER</strong></p>
                  <p>A SEMANA DO HAMBURGUER é evento criado pela KPP ORGANIZAÇÃO DE EVENTOS LTDA. – CNPJ: 05737109000120 –, que tem domicílio na Rua Capitão Antônio Roda, nº 409, Jardim Paulistano, São Paulo/SP. </p>
                  <p>Somos uma plataforma de promoção dos esforços delivery das hamburguerias e restaurantes aqui listados. Nossa missão é promover uma inciativa de apoio a restaurantes da cidade de São Paulo e, através desta promoção, conectá-los a consumidores, incentivando a prática de delivery.</p>
                  <p>Para nós, é fundamental que o usuário participante da promoção tenha uma experiência dinâmica e segura. Neste processo, é importante que, ao navegar por este site e participar da promoção, o usuário esteja em acordo com os itens expostos a seguir.</p>
                  <br/>
                  <p><strong>2. NOSSOS SERVIÇOS</strong></p>
                  <p>Este é um site estritamente promocional, no qual é divulgada a promoção denominada “Semana do Hambúrguer” e onde podem ser encontrados links de acesso aos canais dos restaurantes participantes. Não é realizado qualquer tipo de venda nesta plataforma.</p>
                  <br/>
                  <p><strong>3. NOSSAS POLÍTICAS DE PRIVACIDADE E DE COOKIES</strong></p>
                  <p>Não são coletados quaisquer tipos de dados dos usuários que navegam neste site. Esta plataforma tem função exclusivamente divulgacional da promoção “Semana do Hambúrguer”. </p>
                  <br/>
                  <p><strong>4. REGRAS DE USO</strong></p>
                  <p>Em troca de nossos compromissos, exigimos que você se comprometa com as seguintes regras:</p>
                  <p>▪ É proibido o uso desta plataforma ou de conteúdo nela presente para a realização de atividades ilícitas, enganosas, fraudulentas ou com finalidade ilegal ou não autorizada;</p>
                  <p>▪ É proibido violar as informações da plataforma e dos restaurantes participantes constantes no site, inclusive direitos de propriedade intelectual (quaisquer que sejam, registrados ou não), direito de imagem, direitos contratuais ou quaisquer direitos;</p>
                  <p>▪ É proibido fazer uso de quaisquer informações ou conteúdo contido neste site para fins comerciais sem a nossa autorização por escrito;</p>
                  <p>▪ É proibido insinuar ou declarar que você é afiliado ou endossado pela ”Semana do Hambúrguer e/ou Burger Fest"  sem o nosso consentimento expresso;</p>
                  <p>▪ É proibido incentivar ou promover qualquer atividade que viole estes Termos.</p>
                  <br/>
                  <p><strong>6. LINKS EXTERNOS PARA OUTROS SITES</strong></p>
                  <p>Como cumprimento da finalidade dos Serviços aqui oferecidos, serão fornecidos links de acesso a sites de restaurantes participantes e plataformas parceiras. É de suma importância que o usuário ao acessar os ditos sites e plataformas, leia os seus respectivos termos de uso e políticas de privacidade. Nós não nos responsabilizamos pelas condutas de terceiros e não temos controle nem propriedade para intervir ou resolver eventuais conflitos de qualquer natureza com terceiros. </p>
                  <br/>
                  <p><strong>7. PROPRIEDADE INTELECTUAL</strong></p>
                  <p>Todos os direitos relativos ao BURGER FEST / SEMANA DO HAMBÚRGUER, incluindo mas não se limitando às informações de negócio, layouts, marcas nominativas e mistas, textos, gráficos, ícones, imagens, vídeos e banco de dados, registráveis ou não, são de nossa propriedade, e protegidos pelas Leis e tratados nacionais e internacionais de direito autoral, propriedade industrial e concorrência. O uso de qualquer destes itens de propriedade intelectual de nossa titularidade, de restaurantes participantes ou de plataformas parceiras é terminantemente proibido, exceto se houver expressa autorização para tanto.  </p>
                  <br/>
                  <p><strong>8. RESPONSABILIDADES</strong></p>
                  <p>Os Serviços por nós disponibilizados têm viés estritamente promocional e divulgacional da promoção e de sites dos restaurantes participantes. Por isso, o site SEMANA DO HAMBURGUER não tem responsabilidade pelos serviços disponibilizados e prestados pelas plataformas parceiras e pelos restaurantes participantes. Como consequência, a SEMANA DO HAMBURGUER não tem qualquer responsabilidade por pagamentos e transações financeiras realizadas com terceiros, com coleta, armazenamento e uso de dados, ou por atitudes tomadas por terceiros no preparo e na entrega de pedidos aos usuários, incluindo quaisquer eventualidades com empresas e aplicativos de entrega.</p>
                  <br/>
                  <p><strong>9. EVENTUAIS DISPUTAS</strong></p>
                  <p>Na improvável hipótese de um litígio judicial, já se estabelece que serão competentes os Tribunais da cidade de São Paulo, Estado de São Paulo, Brasil, em conformidade com a legislação brasileira.</p>
                  <br/>
                  <p><strong>10. DISPOSIÇÕES FINAIS</strong></p>
                  <p>▪ Nossos direitos e obrigações podem ser transferidos a outras pessoas. Por exemplo, isso poderia ocorrer em caso de alteração de propriedade (como em uma fusão, aquisição ou venda de ativos) ou de acordo com a Lei;</p>
                  <p>▪ Não somos responsáveis pelos serviços e recursos oferecidos por plataformas parceiras, restaurantes participantes ou terceiros, mesmo que você os acesse por meio de nossos Serviços.</p>
                  <p>▪ A promoção intitulada “burger 10 reais” é válida somente no dia 28 de maio de 2022 nas casas que oferecem esta promoção e enquanto durarem os estoques.</p>
                  <br/>
                  <p><strong>11. INFORMAÇÕES DE CONTATO</strong></p>
                  <p>Se você tiver quaisquer dúvidas sobre estes Termos de Uso e Política de Privacidade, por favor, nos contate através do email: atendimento@krpnet.com.br</p>
                  <br/><br/>
                </div>
              </div>
            </div>
          </div>
        </section>

        <Footer />
      </main>
    );
  }
}
