window.onload = function() {
  // Unix timestamp (in seconds) to count down to
  var twoDaysFromNow = (new Date( 2021, 04, 28, 00, 00, 00, 00 ).getTime() / 1000) + (86400 * 2) + 1;

  console.log( twoDaysFromNow );

  // Set up FlipDown
  var flipdown = new FlipDown(twoDaysFromNow,{
      theme: "light",
      headings: ["Dias", "Horas", "Minutos", "Segundos"]
    })
    .start()
    .ifEnded(() => {
      console.log('The countdown has ended!');
    });
}
